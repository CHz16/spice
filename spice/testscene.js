// testscene.js
// Controller for a scene to test animations and stuff.

function TestSceneController() {
    this.tick = function(timestamp) {
        
    }

    this.tempoChanged = function(oldTempo) {
        
    }
    
    this.paused = function(pauseTime) {
        
    }
    
    this.resumed = function(pausedDuration) {
        
    }
    
    this.wantsDiagonals = true;
    
    this.objects = [
        new Arrow({centerX: 175, centerY: 175, width: 8, height: 14, rightHead: 3, color: "blue"}),
        new GraphicalChart({x: 50, y: 50, height: 500, chart: levels[currentLevel].chart, chartDelay: levels[currentLevel].chartDelay, scroll: .8, intro: levels[currentLevel].intro, barLength: levels[currentLevel].barLength})
    ];
    
}

TestSceneController.prototype.draw = function(timestamp) {
    
}

TestSceneController.prototype.startTransitionIn = function(transitionType) {
    objectManager.add(this.objects);
}

TestSceneController.prototype.finishTransitionIn = function(transitionType) {
    

}

TestSceneController.prototype.startTransitionOut = function(transitionType) {
    objectManager.remove(this.objects);
}

TestSceneController.prototype.finishTransitionOut = function(transitionType) {
    
}

TestSceneController.prototype.keyDown = function(key, timestamp) {
    document.getElementById("status").innerHTML = key;
}
