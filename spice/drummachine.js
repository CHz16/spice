// drummachine.js
// Programmable drum machine.

function DrumMachine(drumBuffers, context) {
    this.tick = function(timestamp) {
        if (this.isPaused) {
            return;
        }
        
        // magic number
        while (this.scheduledNodeSets.length > 0 && this.scheduledNodeSets[0].beat.timestamp() < timestamp - 1000) {
            this.popTopNode();
            if (!this.isLooping && this.scheduledNodeSets.length == 0) {
                this.stop();
            }
            
            if (this.isLooping) {
                this.scheduleNextNodeSet();
            }
        }
    }

    this.tempoChanged = function(oldTempo) {
        this.nodeSetsToReschedule = this.scheduledNodeSets.slice(0);
        this.clearNodes();
        
        for (var i = 0; i < this.nodeSetsToReschedule.length; i++) {
            var nodeSet = this.nodeSetsToReschedule[i];
            if (nodeSet.beat.rawTimeFromNow() > 0) {
                nodeSet.beat = nodeSet.beat.reinterpretWithCurrentMetronome();
            }
        }
        this.rescheduleNodeSets();
    }
    
    this.paused = function(pauseTime) {
        this.isPaused = true;
        this.nodeSetsToReschedule = this.scheduledNodeSets.slice(0);
        this.clearNodes();
    }
    
    this.resumed = function(pausedDuration) {
        this.isPaused = false;
        for (var i = 0; i < this.nodeSetsToReschedule.length; i++) {
            this.nodeSetsToReschedule[i].beat.startTime += pausedDuration;
            this.nodeSetsToReschedule[i].beat.cachedTimestamp = undefined;
        }
        this.rescheduleNodeSets();
    }
    
    this.clearNodes = function() {
        while (this.scheduledNodeSets.length > 0) {
            this.popTopNode();
        }
    }
    
    this.popTopNode = function() {
        var nodes = this.scheduledNodeSets[0].nodes;
        for (var i = 0; i < nodes.length; i++) {
            try {
                nodes[i].stop();
            } catch (e) {
                // Catching InvalidStateError if the node hasn't been started
                // yet. This will happen when rescheduling a node that wasn't
                // started because it already played but hasn't been culled yet.
            }
            nodes[i].disconnect();
        }
        return this.scheduledNodeSets.splice(0, 1);
    }
    
    this.scheduleNextNodeSet = function() {
        var drumEvent;
        if (this.intro !== undefined) {
            drumEvent = this.intro[0];
            this.intro.splice(0, 1);
            if (this.intro.length == 0) {
                this.intro = undefined;
            }
        } else {
            drumEvent = this.pattern[0];
            this.pattern.push(drumEvent);
            this.pattern.splice(0, 1);
        }
        
        this.scheduleNodes(drumEvent, this.currentBeat);
        this.currentBeat = this.currentBeat.advancedBy(drumEvent.duration);
    }
    
    this.scheduleNodes = function(drumEvent, beat) {
        var timestamp = this.context.currentTime + beat.rawTimeFromNow() / 1000;
        var nodes = [];
        for (var i = 0; i < drumEvent.channels.length; i++) {
            var source = this.context.createBufferSource();
            nodes.push(source);
            source.buffer = this.drumBuffers[drumEvent.channels[i]];
            source.connect(this.context.destination);
            if (timestamp < this.context.currentTime) {
                try {
                    source.start(0, this.context.currentTime - timestamp);
                } catch (e) {
                    // Catching InvalidStateError if the offset is larger than
                    // the duration of the sound, which will happen if the node
                    // has finished playing but hasn't been culled yet.
                }
            } else {
                source.start(timestamp);
            }
        }
        this.scheduledNodeSets.push({beat: beat, drumEvent: drumEvent, nodes: nodes});
    }
    
    this.rescheduleNodeSets = function() {
        for (var i = 0; i < this.nodeSetsToReschedule.length; i++) {
            var nodeSet = this.nodeSetsToReschedule[i];
            this.scheduleNodes(nodeSet.drumEvent, nodeSet.beat);
        }
        
        var lastNodeSet = this.nodeSetsToReschedule[this.nodeSetsToReschedule.length - 1];
        this.currentBeat = lastNodeSet.beat.advancedBy(lastNodeSet.drumEvent.duration);
        this.nodeSetsToReschedule = undefined;
    }

    this.context = context;
    this.drumBuffers = [];
    var i = 0;
    while (drumBuffers[i] !== undefined) {
        this.drumBuffers.push(drumBuffers[i]);
        i += 1;
    }
    
    this.isPaused = false, this.isLooping = true;
    this.intro = undefined, this.pattern = undefined;
    this.scheduledNodeSets = [], this.nodeSetsToReschedule = undefined;
    this.currentBeat = undefined;
}

DrumMachine.prototype.loadPattern = function(pattern, intro, loop) {
    this.stop();
    
    if (loop === undefined) {
        this.isLooping = true;
    } else {
        this.isLooping = loop;
    }
    
    if (intro) {
        this.intro = intro.slice(0); // Copy because we rotate it internally
    } else {
        this.intro = undefined;
    }
    this.pattern = pattern.slice(0); // Copy because we rotate it internally
}

DrumMachine.prototype.start = function(startBeat) {
    metronome.registerListener(this);
    
    this.currentBeat = startBeat;
    
    var setLimit;
    if (this.isLooping) {
        // magic number
        setLimit = 20;
    } else {
        setLimit = this.pattern.length;
        if (this.intro !== undefined) {
            setLimit += this.intro.length;
        }
    }
    for (var i = 0; i < setLimit; i++) {
        this.scheduleNextNodeSet();
    }
}

DrumMachine.prototype.stop = function() {
    metronome.unregisterListener(this);
    
    this.clearNodes();
}


//
// UTILITY DURATION CALCULATION
//

function patternDuration(pattern, manualDelay) {
    var stepSizes = [];
    stepSizes[Note.thirtysecond] = 1;
    stepSizes[Note.sixteenth] = 2;
    stepSizes[Note.eighth] = 4;
    stepSizes[Note.quarter] = 8;
    stepSizes[Note.half] = 16;
    stepSizes[Note.whole] = 32;
    
    var thirtyseconds = 0;
    if (manualDelay !== undefined) {
        for (var i = 0; i < manualDelay.length; i++) {
            thirtyseconds += stepSizes[manualDelay[i]];
        }
    } else {
        for (var i = 0; i < pattern.length; i++) {
            if (Array.isArray(pattern[i].duration)) {
                for (j = 0; j < pattern[i].duration.length; j++) {
                    thirtyseconds += stepSizes[pattern[i].duration[j]];
                }
            } else {
                thirtyseconds += stepSizes[pattern[i].duration];
            }
        }
    }
    return thirtyseconds;
}
