// numbers.js
// Some numbery things.

function randomIntInRange(min, max) {
    return min + Math.floor((max - min + 1) * Math.random());
}

function randomSign() {
    return randomIntInRange(0, 1) ? -1 : 1;
}

function randomFloatInRange(min, max) {
    return min + (max - min) * Math.random();
}

function clamp(n, min, max) {
    if (n < min) {
        return min;
    } else {
        return Math.min(n, max);
    }
}
