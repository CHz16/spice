// titlescene.js
// Controller for the game scene.

var mainMenuX = 450, mainMenuStartY = 375, mainMenuLineHeight = 75;
var mainMenuFont = "30px sans-serif";
var mainMenuRestartCutoff = 2.5; // seconds

var mainMenuStockPhotoX = -100, mainMenuStockPhotoY = 100;
var mainMenuStockPhotoData = [
    {xMotion: -100, yMotion: -300, rotate: 10},
    {xMotion: 250, yMotion: 100, rotate: -15},
    {xMotion: 200, yMotion: -200, rotate: 15},
    {xMotion: -300, yMotion: 150, rotate: -10}
];

function TitleSceneController() {
    this.tick = function(timestamp) {
        if (this.menuMusicStartTime !== undefined && this.menuMusicStartTime + soundPlayer.soundBuffers["menu"].duration - mainMenuRestartCutoff <= audioContext.currentTime) {
            this.menuMusicStartTime = undefined;
            this.restarting = true;
            this.restartTime = timestamp + dvdSeekDuration;
            objectManager.remove(this.selectionArrow);
            soundPlayer.stop("menu");
        }
        if (!this.isPaused && this.restartTime !== undefined && this.restartTime <= timestamp) {
            this.restarting = false;
            this.restartTime = undefined;
            objectManager.add(this.selectionArrow);
            soundPlayer.play("menu");
            this.startMusicCounter(0);
            this.currentStockPhoto = 0;
            this.resetStockPhotos();
            this.animateStockPhotoIn();
        }
    }

    this.tempoChanged = function(oldTempo) {
        
    }
    
    this.paused = function(pauseTime) {
        this.isPaused = true;
        
        if (this.restarting) {
            return;
        }
        this.menuMusicPlayedTime = audioContext.currentTime - this.menuMusicStartTime;
        this.menuMusicStartTime = undefined;
    }
    
    this.resumed = function(pausedDuration) {
        this.isPaused = false;
        if (this.restarting) {
            this.restartTime += pausedDuration;
            return;
        }
        this.startMusicCounter(this.menuMusicPlayedTime);
        this.menuMusicPlayedTime = undefined;
    }
    
    this.startMusicCounter = function(offset) {
        this.menuMusicStartTime = audioContext.currentTime - offset;
    }
    
    this.moveSelectionArrow = function() {
        this.selectionArrow.centerY = mainMenuStartY - 10 + (this.selection * mainMenuLineHeight);
    }
    
    this.resetStockPhotos = function() {
        for (var i = 0; i < this.stockPhotos.length; i++) {
            var data = mainMenuStockPhotoData[i];
            var photo = this.stockPhotos[i];
            
            photo.x = mainMenuStockPhotoX - data.xMotion;
            photo.y = mainMenuStockPhotoY - data.yMotion;
            photo.alpha = 0.0;
        }
    }
    
    this.animateStockPhotoIn = function() {
        objectManager.add(this.stockPhotos[this.currentStockPhoto]);
        
        var now = metronome.currentBeat(Note.thirtysecond);
        var end = now.advancedBy(Note.whole)
        animationManager.makeTween({
            object: this.stockPhotos[this.currentStockPhoto],
            property: "x", endValue: mainMenuStockPhotoX,
            startBeat: now, endBeat: end,
            callback: function() {
                this.animateStockPhotoOut(this.currentStockPhoto);
                this.currentStockPhoto += 1;
                if (this.currentStockPhoto >= this.stockPhotos.length) {
                    this.currentStockPhoto = undefined;
                } else {
                    this.animateStockPhotoIn();
                }
            }.bind(this)
        });
        animationManager.makeTween({
            object: this.stockPhotos[this.currentStockPhoto],
            property: "y", endValue: mainMenuStockPhotoY,
            startBeat: now, endBeat: end
        });
        animationManager.makeTween({
            object: this.stockPhotos[this.currentStockPhoto],
            property: "alpha", endValue: 0.5,
            startBeat: now, endBeat: end,
            easing: easeOutQuad
        });
    }
    
    this.animateStockPhotoOut = function(stockPhoto) {
        var data = mainMenuStockPhotoData[stockPhoto];
        var now = metronome.currentBeat(Note.thirtysecond);
        var end = now.advancedBy(Note.whole)
        animationManager.makeTween({
            object: this.stockPhotos[stockPhoto],
            property: "x", endValue: mainMenuStockPhotoX + data.xMotion,
            startBeat: now, endBeat: end,
            callback: function() {
                objectManager.remove(this.stockPhotos[stockPhoto]);
            }.bind(this)
        });
        animationManager.makeTween({
            object: this.stockPhotos[stockPhoto],
            property: "y", endValue: mainMenuStockPhotoY + data.yMotion,
            startBeat: now, endBeat: end
        });
        animationManager.makeTween({
            object: this.stockPhotos[stockPhoto],
            property: "alpha", endValue: 0.0,
            startBeat: now, endBeat: end,
            easing: easeInQuad
        });
    }
    
    this.logo = new Bitmap({x: 200, y: 32, image: imagePreloader.getImage("logo"), zIndex: 10});
    
    this.menuOptions = [
        new Label({x: mainMenuX, y: mainMenuStartY, font: mainMenuFont, text: "PLAY", zIndex: 10}),
        new Label({x: mainMenuX, y: mainMenuStartY + mainMenuLineHeight, font: mainMenuFont, text: "SCENE SELECTION", zIndex: 10}),
        new Label({x: mainMenuX, y: mainMenuStartY + 2 * mainMenuLineHeight, font: mainMenuFont, text: "SET UP", zIndex: 10})
    ];
    
    this.selection = 0;
    this.selectionArrow = new Arrow({centerX: mainMenuX - 40, centerY: 0, width: 30, height: 15, color: "blue", rightHead: 5, zIndex: 10});
    
    this.menuMusicSource = undefined;
    this.menuMusicStartTime = undefined, this.menuMusicPlayedTime = undefined;
    
    this.isPaused = false;
    this.restarting = false, this.restartTime = undefined;
    
    this.stockPhotos = [], this.currentStockPhoto = undefined;
    for (var i = 0; i < mainMenuStockPhotoData.length; i++) {
        this.stockPhotos.push(new Bitmap({x: 0, y: 0, image: imagePreloader.getImage("stockphoto" + (i + 1)), rotate: mainMenuStockPhotoData[i].rotate, zIndex: 0}));
    }
}

TitleSceneController.prototype.draw = function(timestamp) {
    
}

TitleSceneController.prototype.startTransitionIn = function(transitionType) {
    this.menuMusicStartTime = undefined;
    this.menuMusicPlayedTime = undefined;
    this.isPaused = false;
    this.restarting = false;
    this.restartTime = undefined;
    
    this.selection = 0;
    this.moveSelectionArrow();
    
    this.currentStockPhoto = 0;
    this.resetStockPhotos();
    
    if (currentLevel !== undefined) {
        this.menuOptions[0].text = "RESUME";
    } else {
        this.menuOptions[0].text = "PLAY";
    }
}

TitleSceneController.prototype.finishTransitionIn = function(transitionType) {
    objectManager.add(this.logo);
    objectManager.add(this.menuOptions);
    objectManager.add(this.selectionArrow);
    
    metronome.setTempo(50);
    metronome.registerListener(this);
    this.startMusicCounter(0);
    soundPlayer.play("menu");
    this.animateStockPhotoIn();
}

TitleSceneController.prototype.startTransitionOut = function(transitionType) {
    metronome.unregisterListener(this);
    soundPlayer.stop("menu");
    objectManager.remove(this.selectionArrow);
    animationManager.unscheduleAnimationsFor(this.stockPhotos);
}

TitleSceneController.prototype.finishTransitionOut = function(transitionType) {
    objectManager.remove(this.logo);
    objectManager.remove(this.menuOptions);    
    objectManager.remove(this.stockPhotos);
}

TitleSceneController.prototype.keyDown = function(key, timestamp) {
    if (this.restarting) {
        return;
    }
    
    if (key == Key.up) {
        this.selection = (this.selection + 2) % 3;
        this.moveSelectionArrow();
    } else if (key == Key.down) {
        this.selection = (this.selection + 1) % 3;
        this.moveSelectionArrow();
    } else if (key == Key.action) {
        if (this.selection == 0) {
            startTransition(1, Transition.seek);
        } else if (this.selection == 1) {
            startTransition(5, Transition.seek);
        } else if (this.selection == 2) {
            startTransition(6, Transition.seek);
        }
    }
}
