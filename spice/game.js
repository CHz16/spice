// game.js
// Spice main game code.


//
// GAME PARAMETERS
//

var gameWidth = 800, gameHeight = 600;

var inputError = 75; // milliseconds on either side of the beat
var diagonalInputTolerance = 50; // milliseconds

var backgroundColor = "white", frameColor = "black";
var frameSize = 40, bulgeSize = 15; // pixels
var arrowheadStretchFactor = 4;

var dvdSeekDuration = 1500; // milliseconds
var dvdSeekStartTime;
var dvdMinScale = 0.1;

var sceneStartDelay = 1000; // milliseconds, used for watching & playing scenes


//
// GLOBALS
//

var audioContext;
var canvas, canvasContext;
var metronome, objectManager, animationManager, drumMachine, chartManager;
var soundPlayer;
var imagePreloader, soundPreloader;

var isLoaded = false;
var scenes = [], currentScene = 0; // 0 = title, 1 = watching, 2 = playing, 3 = ending, 4 = credits, 5 = scene select, 6 = setup, 7 = test
var currentLevel;
var allowedMistakes = 1;

var isPaused = false, pauseTime;
var currentTransitionType, seekEndTime, deferredDestination;
var pendingDirection, pendingDirectionTimestamp, pendingDirectionTimeout;


//
// INITIALIZATION
//

window.onload = init;
function init() {
    let canvasThings = makeScaledCanvas(gameWidth, gameHeight);
    canvas = canvasThings.canvas;
    canvasContext = canvasThings.context;

    // Replace HTML canvas with new canvas and set its display size
    document.body.replaceChild(canvas, document.getElementById("canvas"));
    canvas.style.width = gameWidth + "px";
    canvas.style.height = gameHeight + "px";

    canvas.onmouseup = function(evt) {
        canvas.onmouseup = undefined;
        init2();
    }

    drawLoadingMessage("Click to play");
}

function init2() {
    audioContext = new (window.AudioContext || window.webkitAudioContext)();
    audioContext.resume(); // Safari 12 autoplay policy pls

    var loadCallback = function(preloader, soundsHandled, index) {
        drawLoadingMessage("Loaded sound " + soundsHandled + "/" + preloader.sounds.length);
        if (soundsHandled == preloader.sounds.length) {
            soundPlayer = new SoundPlayer(preloader.soundBuffers, audioContext);
            init3();
        }
    };
    var errorCallback = function(preloader, soundsHandled, index) {
        drawLoadingMessage("Whoopsie, couldn't load the sound: " + preloader.sounds[index].key);
        return true;
    };

    var drumChannels = [
        "hihat-open",   // 0
        "snare2",       // 1
        "cowbell",      // 2
        "bass",         // 3
        "hihat-closed", // 4
        "tom1",         // 5
        "tom3",         // 6
        "tom4",         // 7
        "crash",        // 8
        "tom2",         // 9
        "ride-strong",  // 10
        "snare1",       // 11
        "cowbell-light" // 12
    ];
    var sounds = [];
    for (var i = 0; i < drumChannels.length; i++) {
        sounds.push({key: i, urls: ["drums/" + drumChannels[i] + ".ogg", "drums/" + drumChannels[i] + ".mp3"]});
    }
    sounds.push(
        {key: "menu", urls: ["music/menu.ogg", "music/menu.mp3"]},
        {key: "victory", urls: ["music/victory.ogg", "music/victory.mp3"]},
        {key: "hellloop", urls: ["music/hellloop.ogg", "music/hellloop.mp3"]},
        {key: "credits", urls: ["music/credits.ogg", "music/credits.mp3"]}
    );
    soundPreloader = new SoundPreloader(sounds, audioContext, {loadCallback: loadCallback, errorCallback: errorCallback});
    drawLoadingMessage("Requesting sounds");

    canvas.tabIndex = 0;
    canvas.addEventListener("keydown", keyDownHandler);
    canvas.addEventListener("focus", function() {
        if (isLoaded) {
            window.requestAnimationFrame(draw);
        }
    });
    canvas.addEventListener("blur", function(event) { canvas.blur(); });
    if (document.hasFocus()) {
        canvas.focus();
    }
}

function init3() {
    var loadCallback = function(preloader, imagesHandled, index) {
        drawLoadingMessage("Loaded image " + imagesHandled + "/" + preloader.keys.length);
        if (imagesHandled == preloader.urls.length) {
            init4();
        }
    };
    var errorCallback = function(preloader, imagesHandled, index) {
        drawLoadingMessage("Whoopsie, couldn't load the image: " + preloader.keys[index]);
        return true;
    };

    imagePreloader = new ImagePreloader(["stockphoto1.jpg", "stockphoto2.jpg", "stockphoto3.jpg", "stockphoto4.jpg", "logo.png", "heart.png", "star.png"], {base: "images/", keys: ["stockphoto1", "stockphoto2", "stockphoto3", "stockphoto4", "logo", "heart", "star"], loadCallback: loadCallback, errorCallback: errorCallback});
    drawLoadingMessage("Requesting images");
}

function init4() {
    scenes.push(new TitleSceneController());
    scenes.push(new WatchingSceneController());
    scenes.push(new PlayingSceneController());
    scenes.push(new EndingSceneController());
    scenes.push(new CreditsSceneController());
    scenes.push(new SelectionSceneController());
    scenes.push(new SetupSceneController());
    if (typeof TestSceneController !== "undefined") {
        scenes.push(new TestSceneController());
    }
    isLoaded = true;

    window.requestAnimationFrame(draw);
}

function drawLoadingMessage(text) {
    canvasContext.fillStyle = backgroundColor;
    canvasContext.fillRect(0, 0, gameWidth, gameHeight);

    canvasContext.font = "30px sans-serif";
    canvasContext.fillStyle = "black";
    var metrics = canvasContext.measureText(text);
    canvasContext.fillText(text, Math.round((gameWidth - metrics.width) / 2), 300);
}


//
// DRAWING
//

// Creates a canvas and 2D drawing context for that canvas that are set up for HiDPI drawing.
// Be careful not to undo the base scale transform on the canvas context with .resetTransform() or anything
// Also be careful to, when you're compositing canvases with .drawImage(), specify the "base" width and height of the canvas and not the scaled size
function makeScaledCanvas(width, height) {
    let scale = window.devicePixelRatio || 1;

    let canvas = document.createElement("canvas");
    canvas.width = width * scale;
    canvas.height = height * scale;

    let context = canvas.getContext("2d");
    context.scale(scale, scale);
    context.imageSmoothingEnabled = false;

    return {canvas: canvas, context: context};
}

function draw(timestamp, noupdate) {
    if (metronome === undefined) {
        metronome = new Metronome(60);
        objectManager = new ObjectManager();
        animationManager = new AnimationManager();
        drumMachine = new DrumMachine(soundPreloader.soundBuffers, audioContext);
        soundPreloader = undefined;
        chartManager = new ChartManager();

        metronome.start(timestamp);
        scenes[currentScene].startTransitionIn(Transition.immediate);
        scenes[currentScene].finishTransitionIn(Transition.immediate);
    } else if (!noupdate) {
        metronome.tick();
    }

    if (isPaused) {
        if (currentTransitionType === Transition.seek) {
            seekEndTime += timestamp - pauseTime;
        }
        isPaused = false;
        pauseTime = undefined;
        metronome.resume();
    }

    if (currentTransitionType === Transition.seek) {
        if (seekEndTime === undefined) {
            seekEndTime = timestamp + dvdSeekDuration;
        } else if (seekEndTime <= timestamp) {
            seekEndTime = undefined;
            finishTransition();
        }
    }

    canvasContext.fillStyle = backgroundColor;
    canvasContext.fillRect(0, 0, gameWidth, gameHeight);
    objectManager.drawEverything(canvasContext);
    scenes[currentScene].draw(timestamp);

    if (document.activeElement != canvas) {
        isPaused = true;
        pauseTime = timestamp;

        canvasContext.fillStyle = "rgba(0, 0, 0, 0.5)";
        canvasContext.fillRect(0, 0, gameWidth, gameHeight);

        canvasContext.font = "32px sans-serif";
        var metrics = canvasContext.measureText("(click to continue)");
        var x = Math.round((gameWidth - metrics.width) / 2);
        canvasContext.fillStyle = "#222222";
        canvasContext.fillRect(x - 10, 265, metrics.width + 20, 50);
        canvasContext.fillStyle = "white";
        canvasContext.fillText("(click to continue)", x, 300);

        metronome.pause();
    } else if (!noupdate) {
        window.requestAnimationFrame(draw);
    }
}


//
// SCENE TRANSITIONS
//

var Transition = {immediate: 0, seek: 1, zoomIn: 2, zoomOut: 3};

function startTransition(destination, transitionType) {
    if (transitionType === undefined) {
        transitionType = immediate;
    }

    scenes[currentScene].startTransitionOut(transitionType);
    scenes[destination].startTransitionIn(transitionType);

    currentTransitionType = transitionType;
    deferredDestination = destination;
    if (currentTransitionType == Transition.immediate) {
        finishTransition();
    } else if (currentTransitionType == Transition.zoomOut) {
        animationManager.makeTween({object: objectManager, property: "dvdScale", endValue: dvdMinScale, startBeat: metronome.currentBeat(Note.thirtysecond), endBeat: metronome.currentBeat(Note.thirtysecond).advancedBy(Note.eighth), callback: finishTransition});
    } else if (currentTransitionType == Transition.zoomIn) {
        animationManager.makeTween({object: objectManager, property: "dvdScale", endValue: 1, startBeat: metronome.currentBeat(Note.thirtysecond), endBeat: metronome.currentBeat(Note.thirtysecond).advancedBy(Note.eighth), callback: finishTransition});
    }
}

function finishTransition() {
    scenes[currentScene].finishTransitionOut(currentTransitionType);
    currentScene = deferredDestination;
    scenes[currentScene].finishTransitionIn(currentTransitionType);

    currentTransitionType = undefined;
    deferredDestination = undefined;
}


//
// INPUT
//

// Key enum is defined at the top of leveldata.js because of import order

var keyBindings = [];
keyBindings[38] = Key.up;
keyBindings[40] = Key.down;
keyBindings[37] = Key.left;
keyBindings[39] = Key.right;
keyBindings[90] = Key.action;
keyBindings[88] = Key.cancel;

function keyDownHandler(event) {
    if (!isLoaded) {
        return;
    }

    var timestamp = metronome.currentTime();

    var key;
    if (currentTransitionType === undefined) {
        key = keyBindings[event.keyCode];
    }

    // If there's a pending direction, then the last key hit was an arrow and
    // it was hit when the scene allowed diagonals
    if (pendingDirection !== undefined) {
        var mergedDirection = mergeDirections(pendingDirection, key);
        if (!scenes[currentScene].wantsDiagonals || mergedDirection === undefined) {
            // If the scene no longer wants diagonals or if the key that was
            // just hit isn't combinable into a diagonal, then send the pending
            // direction at the time it was hit.
            sendToScene(pendingDirection, pendingDirectionTimestamp);
        } else {
            // Otherwise, combine the current key with the pending direction
            // into a diagonal and send that. This clears the current key too.
            sendToScene(mergedDirection, timestamp);
            key = undefined;
            event.preventDefault();
        }
    }

    // If the current scene wants diagonals and the current key is a direction,
    // then save it for now to see if the user hits the other key in a diagonal.
    // Also set a timer so that if the user doesn't hit another key immediately,
    // then the key gets sent after some threshold.
    if (scenes[currentScene].wantsDiagonals && (key === Key.up || key === Key.down || key === Key.left || key === Key.right)) {
        pendingDirection = key;
        pendingDirectionTimestamp = timestamp;
        pendingDirectionTimeout = window.setTimeout(function() {
            sendToScene(pendingDirection, pendingDirectionTimestamp);
        }, diagonalInputTolerance);

        key = undefined;
        event.preventDefault();
    }

    // If we still haven't consumed the current input as either a direction
    // waiting for a diagonal or a combination with the previous direction, then
    // send it off now.
    if (key !== undefined) {
        sendToScene(key, timestamp);
        event.preventDefault();
    }
}

function sendToScene(key, timestamp) {
    clearPendingDirection();
    scenes[currentScene].keyDown(key, timestamp);
}

function clearPendingDirection() {
    pendingDirection = undefined;
    pendingDirectionTimestamp = undefined;
    window.clearTimeout(pendingDirectionTimeout);
}

function mergeDirections(key1, key2) {
    if (key1 !== Key.up && key1 !== Key.down) {
        var temp = key1;
        key1 = key2;
        key2 = temp;
    }

    if (key1 === Key.up && key2 === Key.left) {
        return Key.upleft;
    } else if (key1 === Key.up && key2 === Key.right) {
        return Key.upright;
    } else if (key1 === Key.down && key2 === Key.left) {
        return Key.downleft;
    } else if (key1 === Key.down && key2 === Key.right) {
        return Key.downright;
    }

    return undefined;
}
