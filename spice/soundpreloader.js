// soundpreloader.js
// Object for easy preloading of sounds, with event hooks for actions to take
// place after loading.
//
// This class preloads the list of sounds given in the array sounds. Each
// element of this array represents the data of one sound effect and must be an
// object with the following properties set:
//
//   - key: A key to refer to the sound.
//
//   - urls: An array of files to load. This array should consist of the same
//           sound effect in multiple formats for browser compatibility, so
//           maybe something like ["blah.ogg", "blah.wav"]. This class will load
//           the first file in the array that it guesses the browser can play
//           based on the file extension. If it doesn't think it can play any of
//           them, errorCallback will be called (see below).
//
// The second argument is an audio context to create the audio buffers in.
//
// The third optional argument is an object with any or all of the following
// properties set:
//
//   - base: A base directory for sound URLs. If this property is set, then the
//           sound at sounds[i].url[j] will be fetched from
//           (base + sounds[i].url[j]).
//
//   - loadCallback: A function to call after any sound is loaded. The function
//                   signature should look like this:
//                       function loadCallback(preloader, soundsHandled, index)
//                   preloader is the preloader object, soundsHandled is the
//                   total number of sounds that have loaded or errored, and
//                   index is the index of the sound in sounds that was just
//                   loaded. This function does not need a return value, but if
//                   it returns true, then no further sounds will be loaded.
//
//   - errorCallback: A function to call if there's an error in loading a sound.
//                    The function signature should look like this:
//                        function errorCallback(pl, soundsHandled, index)
//                    pl is the preloader object, soundsHandled is the total
//                    number of sounds that have loaded or errored, and index is
//                    the index of the sound in sounds that just failed to load.
//                    This function does not need a return value, but if it
//                    returns true, then no further sounds will be loaded.
//
//   - allCallback: A function to call after all sounds have either loaded or
//                  errored out. The function signature should look like this:
//                      function allCallback(preloader, soundsHandled, errors)
//                  preloader is the preloader object, soundsHandled is the
//                  total number of sounds that were attempted to be loaded, and
//                  errors is an array containing the keys of all the sounds
//                  that were unable to be loaded. This callback will be called
//                  AFTER loadCallback or errorCallback is called for the final
//                  sound to be loaded. If loadCallback or errorCallback returns
//                  true, this callback will be called with soundsHandled and
//                  errors counting only the sounds that were loaded.
function SoundPreloader(sounds, context, args) {
    this.preloadSound = function(index) {
        // Find the first sound file that the browser can play.
        var soundUrl = "";
        for (var urlIndex = 0; urlIndex < this.sounds[index].urls.length; urlIndex++) {
            var url = this.sounds[index].urls[urlIndex];
            var extension = url.substring(url.lastIndexOf(".") + 1);
            if (this.canPlayType[extension]) {
                soundUrl = url;
                break;
            }
        }
        
        // If the browser can't play any of them, then error.
        if (soundUrl === "") {
            this.soundErrored(index);
            return;
        }
        
        // Otherwise, kick off an AJAX request for the sound.
        var preloader = this;
        var request = new XMLHttpRequest();
        request.open("GET", this.base + soundUrl);
        request.responseType = "arraybuffer";
        request.onload = function(event) { preloader.soundLoaded(index, event); };
        request.onerror = function(event) { preloader.soundErrored(index, event); };
        this.requests[this.sounds[index].key] = request;
        this.statuses[this.sounds[index].key] = SoundPreloadStatus.loading;
        request.send();
    };
    
    this.soundLoaded = function(index, event) {
        var preloader = this;
        this.context.decodeAudioData(event.target.response, function(buffer) {
            if (!preloader.isLoading) {
                return;
            }
            
            var key = preloader.sounds[index].key;
            preloader.soundBuffers[key] = buffer;
            preloader.statuses[key] = SoundPreloadStatus.loaded;
            preloader.soundsHandled += 1;
            
            var stopLoading = preloader.loadCallback(preloader, preloader.soundsHandled, index);
            if (stopLoading || preloader.soundsHandled == preloader.sounds.length) {
                preloader.finishLoading();
            }
        }, function(errorEvent) {
            preloader.soundErrored(index, errorEvent);
        });
    }
    
    this.soundErrored = function(index, event) {
        var key = this.sounds[index].key;
        this.statuses[key] = SoundPreloadStatus.errored;
        this.errors[this.errors.length] = key;
        this.soundsHandled += 1;
        
        var stopLoading = this.errorCallback(this, this.soundsHandled, index);
        if (stopLoading || this.soundsHandled == this.sounds.length) {
            this.finishLoading();
        }
    }
    
    this.finishLoading = function() {
        this.isLoading = false;
        for (var i = 0; i < this.sounds.length; i++) {
            var key = this.sounds[i].key;
            if (this.statuses[key] == SoundPreloadStatus.loading) {
                this.requests[key].abort();
                this.statuses[key] = SoundPreloadStatus.unloaded;
            }
        }
        this.allCallback(this, this.soundsHandled, this.errors);
    }
    
    this.sounds = sounds;
    args = args || new Object();
    this.base = args.base || "";
    this.loadCallback = args.loadCallback || function(preloader, soundsHandled, index) {};
    this.errorCallback = args.errorCallback || function(preloader, soundsHandled, index) {};
    this.allCallback = args.allCallback || function(preloader, soundsHandled, errors) {};

    this.canPlayType = new Object();
    this.context = context;
    this.requests = new Object();
    this.soundBuffers = new Object();
    this.statuses = new Object();
    this.isLoading = false;
    this.soundsHandled = 0;
    this.errors = [];

    // First, we check what kinds of formats the browser can and can't play.
    var audioTester = new Audio();
    this.canPlayType["wav"] = (audioTester.canPlayType("audio/wav") !== "" && audioTester.canPlayType("audio/wav") !== "no");
    this.canPlayType["mp3"] = (audioTester.canPlayType("audio/mpeg") !== "" && audioTester.canPlayType("audio/mpeg") !== "no");
    this.canPlayType["ogg"] = (audioTester.canPlayType("audio/ogg") !== "" && audioTester.canPlayType("audio/ogg") !== "no");
    
    // Now we can kick off preloading.
    this.isLoading = true;
    for (var i = 0; i < sounds.length; i++) {
        this.preloadSound(i);
    }
}
var SoundPreloadStatus = {unloaded: 0, loading: 1, loaded: 2, errored: 3};
