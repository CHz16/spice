// objects.js
// Global manager for onscreen objects with object drawing definitions.

var hellMinOffset = 50, hellMaxOffset = 100;

function ObjectManager() {
    this.objects = [], this.freeObjects = [];
    this.dvdScale = 1;
    this.screenShake = 0;

    this.hellXOffset = 0, this.hellYOffset = 0;
    this.isHellPeeking = false;
    this.pentagram = new Pentagram({centerX: gameWidth / 2, centerY: gameHeight / 2, radius: 200, level: 1});

    // Precalculate the curve data for the TV frame.
    this.curveData = [];
    function arcBetween(x1, y1, x2, y2) {
        // Vector math & trig in here. From the two points and a height above
        // the chord connecting them, we need to calculate the radius and center
        // of the circle and the angles of the two points.

        // Find the radius. One point, the midpoint of the two points, and the
        // center of the circle form a right triangle, so we can use the
        // Pythagorean theorem for this one.
        // d = the distance between the two points. The legs of the triangle are
        // length d/2 and radius - bulgeSize, and the hypotenuse is length r.
        var d2 = (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
        var d = Math.sqrt(d2);
        var r = (d2 + 4 * bulgeSize * bulgeSize) / (8 * bulgeSize);

        // Once we have the radius, we can find the center. The easiest way is
        // to follow the perpendicular bisector of the chord down a length of
        // radius - bulgeSize.
        // A vector for the perpendicular bisector is <y1 - y2, x1 - x2>, which
        // has magnitude d, so we turn it into a unit vector by dividing it by d
        var xc = (x1 + x2) / 2 + (y1 - y2) * (r - bulgeSize) / d;
        var yc = (y1 + y2) / 2 - (x1 - x2) * (r - bulgeSize) / d;

        // Now that we have the circle center, we can find the angles of the two
        // points along the circle with a simple atan2.
        var theta1 = Math.atan2(y1 - yc, x1 - xc);
        var theta2 = Math.atan2(y2 - yc, x2 - xc);

        return {xc: xc, yc: yc, r: r, theta1: theta1, theta2: theta2};
    }
    this.curveData.push(arcBetween(frameSize, frameSize, gameWidth - frameSize, frameSize));
    this.curveData.push(arcBetween(gameWidth - frameSize, frameSize, gameWidth - frameSize, gameHeight - frameSize));
    this.curveData.push(arcBetween(gameWidth - frameSize, gameHeight - frameSize, frameSize, gameHeight - frameSize));
    this.curveData.push(arcBetween(frameSize, gameHeight - frameSize, frameSize, frameSize));
}

ObjectManager.prototype.add = function(object) {
    if (Array.isArray(object)) {
        for (var i = 0; i < object.length; i++) {
            this.add(object[i]);
        }
        return;
    }

    if (object.zIndex === undefined) {
        object.zIndex = 0;
    }

    var i = 0;
    var objects = (object.isFree) ? this.freeObjects : this.objects;
    while (i < objects.length && objects[i].zIndex < object.zIndex) {
        i += 1;
    }
    objects.splice(i, 0, object);
}

ObjectManager.prototype.remove = function(object) {
    if (Array.isArray(object)) {
        for (var i = 0; i < object.length; i++) {
            this.remove(object[i]);
        }
        return;
    }

    for (var i = 0; i < this.objects.length; i++) {
        if (this.objects[i] == object) {
            this.objects.splice(i, 1);
            return;
        }
    }

    for (var i = 0; i < this.freeObjects.length; i++) {
        if (this.freeObjects[i] == object) {
            this.freeObjects.splice(i, 1);
            return;
        }
    }
}

ObjectManager.prototype.drawEverything = function(context) {
    context.save(); // screen shake
    if (this.screenShake != 0) {
        var offset = Math.round(this.screenShake);
        context.translate(randomIntInRange(-offset, offset), randomIntInRange(-offset, offset));
    }

    if (this.isHellPeeking) {
        this.pentagram.draw(context);
    }

    for (var i = 0; i < this.freeObjects.length; i++) {
        if (!this.freeObjects[i].fixedDuringHellPeeks) {
            context.save(); // Hell translation
            context.translate(this.hellXOffset, this.hellYOffset);
            this.freeObjects[i].draw(context);
            context.restore();
        } else {
            this.freeObjects[i].draw(context);
        }
    }

    context.save(); // TV scale
    context.scale(this.dvdScale, this.dvdScale);

    // Frame background
    context.fillStyle = backgroundColor;
    context.fillRect(0, 0, gameWidth, gameHeight);

    // DVD objects
    context.save(); // TV clipping
    context.beginPath();
    context.moveTo(0, 0);
    context.lineTo(gameWidth, 0);
    context.lineTo(gameWidth, gameHeight);
    context.lineTo(0, gameHeight);
    context.clip();
    for (var i = 0; i < this.objects.length; i++) {
        this.objects[i].draw(context);
    }
    context.restore(); // TV clipping

    this.drawFrame(context);

    context.restore(); // TV scale
    context.restore(); // screen shake
}

ObjectManager.prototype.drawFrame = function(context) {
    context.beginPath();
    // Outer rectangle
    context.moveTo(0, 0);
    context.lineTo(gameWidth, 0);
    context.lineTo(gameWidth, gameHeight);
    context.lineTo(0, gameHeight);
    context.closePath();
    // Inner region
    context.moveTo(frameSize, frameSize);
    for (var i = 0; i < this.curveData.length; i++) {
        var curve = this.curveData[i];
        context.arc(curve.xc, curve.yc, curve.r, curve.theta1, curve.theta2);
    }
    context.closePath();
    context.fillStyle = frameColor;
    context.fill("evenodd");
}

ObjectManager.prototype.startHellPeek = function(hellLevel) {
    this.stopHellPeek();

    this.isHellPeeking = true;
    this.hellXOffset = randomSign() * randomIntInRange(hellMinOffset * hellLevel, hellMaxOffset * hellLevel);
    this.hellYOffset = randomSign() * randomIntInRange(hellMinOffset * hellLevel, hellMaxOffset * hellLevel);
    this.pentagram.alpha = hellLevel;
    this.pentagram.level = hellLevel;

    this.pentagram.rotate = randomIntInRange(0, 360);
    now = metronome.rawTime();
    animationManager.makeTween({object: this.pentagram, property: "rotate", endIncrement: 360, startBeat: now, endBeat: now.rawAdvancedBy(3000)});
}

ObjectManager.prototype.stopHellPeek = function() {
    this.isHellPeeking = false;
    this.hellXOffset = 0;
    this.hellYOffset = 0;
    animationManager.unscheduleAnimationsFor(this.pentagram);
}


//
// OBJECTS
//

// Generic stuff

function Group(args) {
    this.x = args.x;
    this.y = args.y;
    this.things = args.things;

    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
    this.zIndex = args.zIndex || 0;
}

Group.prototype.draw = function(context) {
    context.save();
    context.translate(this.x, this.y);
    for (var i = 0; i < this.things.length; i++) {
        this.things[i].draw(context);
    }
    context.restore();
}

function Rectangle(args) {
    this.centerX = args.centerX;
    this.centerY = args.centerY;
    this.width = args.width;
    this.height = args.height;

    this.alpha = (args.alpha !== undefined) ? args.alpha : 1.0;
    this.color = args.color || "black";
    this.rotate = args.rotate || 0;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
    this.zIndex = args.zIndex || 0;
}

Rectangle.prototype.draw = function(context) {
    context.save();
    context.translate(this.centerX, this.centerY);
    context.rotate(this.rotate * Math.PI / 180);
    context.globalAlpha = this.alpha;
    context.fillStyle = this.color;
    context.fillRect(-this.width / 2, -this.height / 2, this.width, this.height);
    context.restore();
}

function Arrow(args) {
    this.drawHead = function(context, headSize) {
        context.beginPath();
        // Inset by one pixel so there isn't a discontinuity when rotated/scaled
        context.moveTo(this.width / 2 - 1, -this.height / 2 - headSize);
        context.lineTo(this.width / 2 + headSize * arrowheadStretchFactor, 0);
        context.lineTo(this.width / 2 - 1, this.height / 2 + headSize);
        context.closePath();
        context.fill();
    }

    this.centerX = args.centerX;
    this.centerY = args.centerY;
    this.width = args.width;
    this.height = args.height;

    this.leftHead = args.leftHead || 0;
    this.rightHead = args.rightHead || 0;
    this.alpha = (args.alpha !== undefined) ? args.alpha : 1.0;
    this.color = args.color || "black";
    this.rotate = args.rotate || 0;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
    this.zIndex = args.zIndex || 0;
}

Arrow.prototype.draw = function(context) {
    context.save();
    context.globalAlpha = this.alpha;

    // Body
    context.translate(this.centerX, this.centerY);
    context.rotate(this.rotate * Math.PI / 180);
    context.fillStyle = this.color;
    context.fillRect(-this.width / 2, -this.height / 2, this.width, this.height);

    // Left head
    if (this.leftHead != 0) {
        context.save();
        context.scale(-1, 1);
        this.drawHead(context, this.leftHead);
        context.restore();
    }

    // Right head
    if (this.rightHead != 0) {
        this.drawHead(context, this.rightHead);
    }

    context.restore();
}

function Label(args) {
    this.x = args.x;
    this.y = args.y;
    this.font = args.font;
    this.text = args.text;

    this.color = args.color || "black";
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
    this.zIndex = args.zIndex || 0;

    if (args.centerX !== undefined) {
        canvasContext.save();
        canvasContext.font = this.font;
        var metrics = canvasContext.measureText(this.text);
        this.x = args.centerX - metrics.width / 2;
        canvasContext.restore();
    }
}

Label.prototype.calculateWidth = function(context) {
    context.save();
    context.font = this.font;
    var metrics = context.measureText(this.text);
    canvasContext.restore();
    return metrics.width;
}

Label.prototype.draw = function(context) {
    context.save();

    context.font = this.font;
    context.fillStyle = this.color;
    context.fillText(this.text, this.x, this.y);

    context.restore();
}

function Bitmap(args) {
    this.x = args.x;
    this.y = args.y;
    this.image = args.image;

    // Mainly here so we can pass in explicit dimensions for a scaled canvas
    this.width = (args.width !== undefined) ? args.width : this.image.width;
    this.height = (args.height !== undefined) ? args.height : this.image.height;

    this.scale = (args.scale !== undefined) ? args.scale : 1.0;
    this.rotate = args.rotate || 0;
    this.alpha = (args.alpha !== undefined) ? args.alpha : 1.0;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
    this.zIndex = args.zIndex || 0;
}

Bitmap.prototype.draw = function(context) {
    context.save();
    context.translate(this.x, this.y);
    context.scale(this.scale, this.scale);
    context.translate(this.width / 2, this.height / 2);
    context.rotate(this.rotate * Math.PI / 180);
    context.globalAlpha = this.alpha;
    context.drawImage(this.image, -this.width / 2, -this.height / 2, this.width, this.height);
    context.restore();
}

// Watching scene

function angleForDirection(direction) {
    switch (direction) {
        case Key.right:
            return 0;
        case Key.downright:
            return 45;
        case Key.down:
            return 90;
        case Key.downleft:
            return 135;
        case Key.left:
            return 180;
        case Key.upleft:
            return 225;
        case Key.up:
            return 270;
        case Key.upright:
            return 315;
        default:
            return undefined;
    }
}

function ChartArrow(args) {
    this.centerX = args.centerX;
    this.centerY = args.centerY;

    this.alpha = (args.alpha !== undefined) ? args.alpha : 1.0;
    this.color = args.color || "black";
    this.rotate = args.rotate || 0;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
    this.zIndex = args.zIndex || 0;
}

ChartArrow.prototype.draw = function(context) {
    context.save();
    context.globalAlpha = this.alpha;
    context.translate(this.centerX, this.centerY);
    context.rotate(this.rotate * Math.PI / 180);

    context.lineWidth = 4;
    context.lineCap = "round";
    context.lineJoin = "round";
    context.strokeStyle = this.color;

    var stretchFactor = (this.rotate % 90 == 0) ? 1 : 1.3;
    context.beginPath();
    context.moveTo(-12 * stretchFactor, 0);
    context.lineTo(12 * stretchFactor, 0);
    context.lineTo(6 * stretchFactor, 6 * stretchFactor);
    context.lineTo(12 * stretchFactor, 0);
    context.lineTo(6 * stretchFactor, -6 * stretchFactor);
    context.stroke();

    context.restore();
}

function GraphicalChart(args) {
    this.plotArrow = function(context, direction, y) {
        var arrow = new ChartArrow({centerX: 18, centerY: y + 16, rotate: angleForDirection(direction), color: "blue"});
        arrow.draw(context);
    }

    this.x = args.x;
    this.y = args.y;
    this.height = args.height;

    this.scroll = (args.scroll !== undefined) ? args.scroll: 0;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
    this.zIndex = args.zIndex || 0;

    var containsSixteenths = false;
    for (var i = 0; i < args.chart.length; i++) {
        if (args.chart[i].duration == Note.sixteenth) {
            containsSixteenths = true;
            break;
        }
    }
    var stepSizes = [];
    stepSizes[Note.thirtysecond] = 0.25;
    stepSizes[Note.sixteenth] = 0.5;
    stepSizes[Note.eighth] = 1;
    stepSizes[Note.quarter] = 2;
    stepSizes[Note.half] = 4;
    stepSizes[Note.whole] = 8;

    var introSteps = patternDuration(args.intro, args.chartDelay) / 4;
    this.introScroll = introSteps * 32;
    var chartSteps = chartDuration(args.chart) / 4;

    this.canvasWidth = 36;
    this.canvasHeight = 32 * chartSteps;
    let canvasThings = makeScaledCanvas(this.canvasWidth, this.canvasHeight);
    this.canvas = canvasThings.canvas;
    let context = canvasThings.context;
    context.clearRect(0, 0, this.canvasWidth, this.canvasHeight);

    // Draw the arrows
    var currentY = 0;
    for (var i = 0; i < args.chart.length; i++) {
        this.plotArrow(context, args.chart[i].direction, currentY);

        if (Array.isArray(args.chart[i].duration)) {
            for (j = 0; j < args.chart[i].duration.length; j++) {
                currentY += 32 * stepSizes[args.chart[i].duration[j]];
            }
        } else {
            currentY += 32 * stepSizes[args.chart[i].duration];
        }
    }

    // Draw the bar lines
    context.save();
    context.lineWidth = 2;
    context.strokeStyle = "black";
    context.setLineDash([6, 3, 4, 3, 4, 3, 4, 3, 6]);
    var barLength = args.barLength || 4;
    for (var y = 0; y <= this.canvasHeight; y += 32 * barLength) {
        context.beginPath();
        context.moveTo(0, y);
        context.lineTo(36, y);
        context.stroke();
    }
    context.restore();

    let spillCanvasThings = makeScaledCanvas(this.canvasWidth, this.canvasHeight);

    this.spillCanvas = spillCanvasThings.canvas;
    var spillContext = spillCanvasThings.context;
    spillContext.drawImage(this.canvas, 0, 0, this.canvasWidth, this.canvasHeight);
    // lmao hack for wrapping arrows
    if (args.chart[args.chart.length - 1].duration == Note.sixteenth) {
        this.plotArrow(spillContext, args.chart[args.chart.length - 1].direction, -16);
    }
}

GraphicalChart.prototype.draw = function(context) {
    context.save();
    context.translate(this.x, this.y);

    context.beginPath();
    context.moveTo(0, 0);
    context.lineTo(36, 0);
    context.lineTo(36, this.height);
    context.lineTo(0, this.height);
    context.closePath();
    context.clip();
    context.beginPath();
    context.moveTo(2, 0);
    context.lineTo(34, 0);
    context.lineTo(34, this.height);
    context.lineTo(2, this.height);
    context.closePath();
    context.fillStyle = "white";
    context.fill();

    var y = 128;
    if (this.scroll >= 0) {
        y -= this.scroll * this.canvasHeight;
    } else {
        y -= this.scroll * this.introScroll;
    }
    var currentCanvas = this.canvas;
    while (y < this.height) {
        context.drawImage(currentCanvas, 0, y, this.canvasWidth, this.canvasHeight);
        y += this.canvasHeight;
        currentCanvas = this.spillCanvas;
    }

    context.lineWidth = 2;
    context.strokeStyle = "black";
    context.beginPath();
    context.moveTo(2, 128);
    context.lineTo(34, 128);
    context.moveTo(34, 160);
    context.lineTo(2, 160);
    context.stroke();

    var topGradient = context.createLinearGradient(16, 0, 16, 128);
    topGradient.addColorStop(0, "rgba(255, 255, 255, 0.85)");
    topGradient.addColorStop(1, "rgba(255, 255, 255, 0)");
    context.fillStyle = topGradient;
    context.fillRect(2, 0, 32, 128);

    var bottomGradient = context.createLinearGradient(16, 160, 16, this.height);
    bottomGradient.addColorStop(0, "rgba(255, 255, 255, 0)");
    bottomGradient.addColorStop(1, "rgba(255, 255, 255, 0.85)");
    context.fillStyle = bottomGradient;
    context.fillRect(2, 160, 32, this.height - 160);

    context.lineWidth = 1;
    context.beginPath();
    context.moveTo(2.5, 0.5);
    context.lineTo(2.5, this.height - 0.5);
    context.moveTo(33.5, this.height - 0.5);
    context.lineTo(33.5, 0.5);
    context.stroke();

    context.restore();
}

// Playing scene

function Health(args) {
    this.x = args.x;
    this.y = args.y;

    this.strike = args.strike || false;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
    this.zIndex = args.zIndex || 0;
}

Health.prototype.draw = function(context) {
    context.save();
    context.translate(this.x, this.y);

    context.fillStyle = "white";
    context.fillRect(0, 0, 30, 60);

    if (this.strike) {
        context.strokeStyle = "red";
        context.lineWidth = 4;
        context.beginPath();
        context.moveTo(0, 0);
        context.lineTo(30, 60);
        context.moveTo(0, 60);
        context.lineTo(30, 0);
        context.stroke();
    }

    context.strokeStyle = "black";
    context.lineWidth = 2;
    context.strokeRect(0, 0, 30, 60);

    context.restore();
}

function Pentagram(args) {
    this.centerX = args.centerX;
    this.centerY = args.centerY;
    this.radius = args.radius;

    this.level = (args.level !== undefined) ? args.level : 1;
    this.rotate = args.rotate || 0;
    this.alpha = (args.alpha !== undefined) ? args.alpha : 1.0;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
    this.zIndex = args.zIndex || 0;
}

Pentagram.prototype.draw = function(context) {
    context.save();
    context.translate(this.centerX, this.centerY);
    context.rotate(this.rotate * Math.PI / 180);
    context.globalAlpha = this.alpha;

    context.lineWidth = 5;
    context.lineJoin = "bevel";
    context.strokeStyle = "red";
    context.beginPath();
    context.moveTo(0, -this.radius);
    context.arc(0, 0, this.radius, 3 * Math.PI / 2, (3 + 4 * this.level) * Math.PI / 2);

    context.moveTo(0, -this.radius);
    var angle = Math.PI / 2;
    var lineRemaining = this.level;
    while (lineRemaining > 0) {
        newAngle = (angle + 4 * Math.PI / 5) % 360;
        var x2 = -this.radius * Math.cos(newAngle);
        var y2 = -this.radius * Math.sin(newAngle);
        if (lineRemaining < .2) {
            var x1 = -this.radius * Math.cos(angle);
            var y1 = -this.radius * Math.sin(angle);
            x2 = x1 + (x2 - x1) * (lineRemaining * 5);
            y2 = y1 + (y2 - y1) * (lineRemaining * 5);
        }
        context.lineTo(x2, y2);

        angle = newAngle;
        lineRemaining -= .2;
    }
    context.stroke();

    context.restore();
}

function VictoryWheel(args) {
    this.centerX = args.centerX;
    this.centerY = args.centerY;
    this.radius = args.radius;

    this.color = args.color || "yellow";
    this.spokes = (args.spokes !== undefined) ? args.spokes : 4;
    this.rotate = args.rotate || 0;
    this.alpha = (args.alpha !== undefined) ? args.alpha : 1.0;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
    this.zIndex = args.zIndex || 0;
}

VictoryWheel.prototype.draw = function(context) {
    context.save();
    context.translate(this.centerX, this.centerY);
    context.rotate(this.rotate * Math.PI / 180);
    context.globalAlpha = this.alpha;
    context.fillStyle = this.color;

    var angle = Math.PI / this.spokes;
    for (var i = 0; i < this.spokes; i++) {
        context.beginPath();
        context.moveTo(0, 0);
        context.arc(0, 0, this.radius, angle * (2 * i), angle * (2 * i + 1));
        context.closePath();
        context.fill();
    }

    context.restore();
}

// Ending scene

function Shard(args) {
    this.x = args.x || 0;
    this.y = args.y || 0;
    this.rotate = args.rotate || 0;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;

    let canvasThings = makeScaledCanvas(gameWidth, gameHeight);
    this.canvas = canvasThings.canvas;
    let context = canvasThings.context;
    context.clearRect(0, 0, gameWidth, gameHeight);

    context.save();
    context.beginPath();
    context.moveTo(args.points[args.polygon[0]].x, args.points[args.polygon[0]].y);
    for (var i = 1; i < args.polygon.length; i++) {
        context.lineTo(args.points[args.polygon[i]].x, args.points[args.polygon[i]].y);
    }
    context.closePath();
    context.clip();
    context.drawImage(args.source, 0, 0, gameWidth, gameHeight);
    context.lineWidth = 1;
    context.stroke();
    context.restore();

    // Calculate centroid
    var a = 0, cx = 0, cy = 0;
    for (var i = 0; i < args.polygon.length; i++) {
        var p = args.points[args.polygon[i]];
        var p1 = args.points[args.polygon[(i + 1) % args.polygon.length]];

        var n = p.x * p1.y - p1.x * p.y;
        cx += (p.x + p1.x) * n;
        cy += (p.y + p1.y) * n;
        a += n;
    }
    this.centroidX = cx / (3 * a);
    this.centroidY = cy / (3 * a);
}

Shard.prototype.draw = function(context) {
    context.save();
    context.translate(this.x, this.y);
    context.translate(this.centroidX, this.centroidY);
    context.rotate(this.rotate * Math.PI / 180);
    context.translate(-this.centroidX, -this.centroidY);
    context.drawImage(this.canvas, 0, 0, gameWidth, gameHeight);
    context.restore();
}

function House(args) {
    this.x = args.x;
    this.y = args.y;

    this.zIndex = args.zIndex || 0;
    this.rotate = args.rotate || 0;
    this.scale = (args.scale !== undefined) ? args.scale : 1.0;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;

    this.width = args.width || 64;
    this.roofHeight = args.roofHeight || 26;
    this.rise = args.rise || 7;
    this.baseHeight = args.baseHeight || 17;
}

House.prototype.draw = function(context) {
    context.save();
    context.translate(this.x, this.y);
    context.scale(this.scale, this.scale);
    var height = this.roofHeight + this.rise + this.baseHeight;
    context.translate(this.width / 2, height / 2);
    context.rotate(this.rotate * Math.PI / 180);
    context.translate(-this.width / 2, -height / 2);

    context.lineWidth = 2 / this.scale;
    context.fillStyle = backgroundColor;
    context.strokeStyle = "black";

    context.beginPath();
    context.moveTo(4, this.roofHeight);
    context.lineTo(4, this.roofHeight + this.rise + this.baseHeight);
    context.lineTo(this.width - 4, this.roofHeight + this.rise + this.baseHeight);
    context.lineTo(this.width - 4, this.roofHeight);
    context.fill();
    context.stroke();

    context.beginPath();
    context.moveTo(0, this.rise);
    context.lineTo(this.width / 2, 0);
    context.lineTo(this.width, this.rise);
    context.lineTo(this.width, this.roofHeight + this.rise);
    context.lineTo(this.width / 2, this.roofHeight);
    context.lineTo(0, this.roofHeight + this.rise);
    context.closePath();
    context.fill();
    context.stroke();

    context.beginPath();
    context.moveTo(this.width / 2, 0);
    context.lineTo(this.width / 2, this.roofHeight);
    context.stroke();

    context.restore();
}

function Fissure(args) {
    this.x = args.x;
    this.y = args.y;
    this.redValue = 80;

    this.zIndex = args.zIndex || 0;
    this.rotate = args.rotate || 0;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
}

Fissure.prototype.draw = function(context) {
    context.save();
    context.translate(this.x, this.y);
    context.translate(57, 11);
    context.rotate(this.rotate * Math.PI / 180);
    context.translate(-57, -11);

    context.beginPath();
    context.moveTo(0, 17);
    context.lineTo(34, 0);
    context.lineTo(67, 7);
    context.lineTo(83, 0);
    context.lineTo(113, 18);
    context.lineTo(88, 12);
    context.lineTo(75, 21);
    context.lineTo(30, 12);
    context.closePath();
    context.fillStyle = "rgb(" + this.redValue + ", 0, 0)";
    context.fill();
    context.lineWidth = 3;
    context.strokeStyle = "black";
    context.stroke();

    context.restore();
}

Fissure.prototype.flicker = function() {
    var newRedValue = this.redValue;
    do {
        newRedValue += randomSign() * randomIntInRange(5, 15);
        newRedValue = clamp(newRedValue, 40, 120);
    } while (newRedValue == this.redValue);
    this.redValue = newRedValue;
}

function Plume(args) {
    this.x = args.x;
    this.y = args.y;
    this.height = args.height;

    this.slope = args.slope || 2;
    this.waveHeight = args.waveHeight || 44;
    this.waveOffset = args.waveOffset || 0;
    this.width = args.width || 100;
    this.alpha = (args.alpha !== undefined) ? args.alpha : 1.0;
    this.zIndex = args.zIndex || 0;
    this.rotate = args.rotate || 0;
    this.isFree = args.isFree || false;
    this.fixedDuringHellPeeks = args.fixedDuringHellPeeks || false;
}

Plume.prototype.draw = function(context) {
    context.save();
    context.globalAlpha = this.alpha;
    context.translate(this.x, this.y);
    context.translate(this.width / 2, this.height / 2);
    context.rotate(this.rotate * Math.PI / 180);
    context.translate(-this.width / 2, -this.height / 2);

    context.beginPath();
    context.moveTo(-2, 0);
    context.lineTo(this.width + 2, 0);
    context.lineTo(this.width + 2, this.height);
    context.lineTo(-2, this.height);
    context.closePath();
    context.clip();

    var offset = this.waveOffset % (this.waveHeight * 2);
    context.translate(0, offset);
    var bottom = this.height;
    if (offset < 0) {
        bottom += this.waveHeight * 2;
    }
    var currentY = bottom;
    context.beginPath();
    context.moveTo(this.waveHeight / this.slope, currentY);
    while (currentY >= -10) {
        currentY -= this.waveHeight;
        context.lineTo(0, currentY);
        currentY -= this.waveHeight;
        context.lineTo(this.waveHeight / this.slope, currentY);
    }
    context.lineTo(this.width, currentY);
    while (currentY < bottom) {
        currentY += this.waveHeight;
        context.lineTo(this.width - this.waveHeight / this.slope, currentY);
        currentY += this.waveHeight;
        context.lineTo(this.width, currentY);
    }
    var gradient = context.createLinearGradient(0, this.height / 2, this.width, this.height / 2);
    gradient.addColorStop(0, "red");
    gradient.addColorStop(0.5, "orange");
    gradient.addColorStop(1, "red");
    context.fillStyle = gradient;
    context.fill();
    context.strokeStyle = "black";
    context.lineWidth = 2;
    context.stroke();

    context.restore();
}
