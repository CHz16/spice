// creditsscene.js
// Controller for the credits scene.

var creditsFont = "30px sans-serif";

function CreditsSceneController() {    
    this.moveGroupIn = function() {
        this.currentGroup += 1;
        if (this.currentGroup == this.groups.length) {
            return;
        }
        
        objectManager.add(this.groups[this.currentGroup]);
        var now = metronome.rawTime();
        animationManager.makeTween({object: this.groups[this.currentGroup], property: "y", endValue: 0, startBeat: now, endBeat: now.rawAdvancedBy(2000), callback: this.groupHold.bind(this)});
    }
    
    this.groupHold = function() {
        animationManager.makeNull({endBeat: metronome.rawTime(8500), callback: this.moveGroupOut.bind(this)});
    }
    
    this.moveGroupOut = function() {
        var cg = this.currentGroup;
        var now = metronome.rawTime();
        animationManager.makeTween({object: this.groups[this.currentGroup], property: "y", endValue: -gameHeight, startBeat: now, endBeat: now.rawAdvancedBy(2000), callback: function() { this.finalizeGroup(cg); }.bind(this)});
        this.moveGroupIn();
    }
    
    this.finalizeGroup = function(i) {
        objectManager.remove(this.groups[i]);
        if (this.currentGroup == this.groups.length) {
            startTransition(0, Transition.seek);
        }
    }
    
    this.currentGroup = -1;
    this.groups = [
        new Group({x: 0, y: 0, things: [
            new Bitmap({x: 200, y: 50, image: imagePreloader.getImage("logo")}),
            new Label({centerX: gameWidth / 2, y: 370, text: "A thing CHz made for Strawberry Jam", font: creditsFont}),
            new Label({centerX: gameWidth / 2, y: 460, text: "Things to blame him for:", font: creditsFont}),
            new Label({centerX: gameWidth / 2, y: 500, text: "Code, childish scribbles, sound & music", font: creditsFont}),
        ]}),
        new Group({x: 0, y: 0, things: [
            new Label({centerX: gameWidth / 2, y: 80, text: "Photo Credits:", font: creditsFont}),
            new Label({x: 110, y: 180, text: "Joanna M. Foto:", font: creditsFont}),
            new Bitmap({x: 360, y: 120, image: imagePreloader.getImage("stockphoto1"), scale: 0.25}),
            new Bitmap({x: 520, y: 120, image: imagePreloader.getImage("stockphoto3"), scale: 0.25}),
            new Label({x: 110, y: 300, text: "fish NC:", font: creditsFont}),
            new Bitmap({x: 360, y: 240, image: imagePreloader.getImage("stockphoto2"), scale: 0.25}),
            new Label({x: 110, y: 420, text: "Club Med UK:", font: creditsFont}),
            new Bitmap({x: 360, y: 360, image: imagePreloader.getImage("stockphoto4"), scale: 0.25}),
            new Label({x: 110, y: 520, text: "And the pink logo frame is by GDJ", font: creditsFont}),
        ]}),
        new Group({x: 0, y: 0, things: [
            new Label({centerX: gameWidth / 2, y: 300, text: "Thanks for playing!", font: creditsFont})
        ]})
    ];
    
    this.heart = new Bitmap({x: 640, y: 440, image: imagePreloader.getImage("heart")});
    this.star = new Bitmap({x: 640, y: 440, image: imagePreloader.getImage("star")});
}

CreditsSceneController.prototype.draw = function(timestamp) {
    
}

CreditsSceneController.prototype.startTransitionIn = function(transitionType) {
    if (allowedMistakes == 0) {
        this.groups[2].things[1] = this.star;
    } else {
        this.groups[2].things[1] = this.heart;
    }
    
    this.currentGroup = -1;
    for (var i = 0; i < this.groups.length; i++) {
        this.groups[i].y = gameHeight;
    }
}

CreditsSceneController.prototype.finishTransitionIn = function(transitionType) {
    soundPlayer.play("credits");
    this.moveGroupIn();
}

CreditsSceneController.prototype.startTransitionOut = function(transitionType) {
    soundPlayer.stop("credits");
}

CreditsSceneController.prototype.finishTransitionOut = function(transitionType) {
    
}

CreditsSceneController.prototype.keyDown = function(key, timestamp) {
    
}
