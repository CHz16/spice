// selectionscene.js
// Controller for the scene selection scene.

var selectionFirstColX = 100, selectionSecondColX = 520, selectionStartY = 110;
var selectionLineHeight = 80, selectionColHeight = 6;
var selectionFont = "30px sans-serif";

function SelectionSceneController() {    
    this.moveSelectionArrow = function() {
        if (this.selection < selectionColHeight) {
            this.selectionArrow.centerX = selectionFirstColX;
            this.selectionArrow.centerY = selectionStartY + this.selection * selectionLineHeight;
        } else {
            this.selectionArrow.centerX = selectionSecondColX;
            this.selectionArrow.centerY = selectionStartY + (this.selection - selectionColHeight) * selectionLineHeight;
        }
        this.selectionArrow.centerX -= 40;
        this.selectionArrow.centerY -= 10;
    }
    
    this.selection = 0;
    this.selectionArrow = new Arrow({centerX: 200, centerY: 200, width: 30, height: 15, color: "blue", rightHead: 5, zIndex: 10});
    
    this.labels = [];
    for (var i = 0; i < levels.length; i++) {
        var label = new Label({text: (i + 1) + ". " + levels[i].name, font: selectionFont});
        if (i < selectionColHeight) {
            label.x = selectionFirstColX;
            label.y = selectionStartY + i * selectionLineHeight;
        } else {
            label.x = selectionSecondColX;
            label.y = selectionStartY + (i - selectionColHeight) * selectionLineHeight;
        }
        this.labels.push(label);
    }
    this.labels.push(new Label({x: selectionSecondColX, y: selectionStartY + (selectionColHeight - 1) * selectionLineHeight, text: "Main Menu", font: selectionFont}));
}

SelectionSceneController.prototype.draw = function(timestamp) {
    
}

SelectionSceneController.prototype.startTransitionIn = function(transitionType) {
    this.selection = 0;
    this.moveSelectionArrow();
}

SelectionSceneController.prototype.finishTransitionIn = function(transitionType) {
    objectManager.add(this.selectionArrow);
    objectManager.add(this.labels);
}

SelectionSceneController.prototype.startTransitionOut = function(transitionType) {
    objectManager.remove(this.selectionArrow);
}

SelectionSceneController.prototype.finishTransitionOut = function(transitionType) {
    objectManager.remove(this.labels);
}

SelectionSceneController.prototype.keyDown = function(key, timestamp) {
    if (key == Key.up) {
        this.selection -= 1;
        if (this.selection == -1 || this.selection == selectionColHeight - 1) {
            this.selection += selectionColHeight;
        } else if (this.selection >= levels.length) {
            this.selection = levels.length - 1;
        }
        this.moveSelectionArrow();
    } else if (key == Key.down) {
        this.selection += 1;
        if (this.selection % selectionColHeight == 0) {
            this.selection -= selectionColHeight;
        } else if (this.selection == levels.length) {
            this.selection = selectionColHeight * 2 - 1;
        }
        this.moveSelectionArrow();
    } else if (key == Key.left || key == Key.right) {
        if (this.selection < selectionColHeight) {
            if (this.selection + selectionColHeight < levels.length || this.selection == selectionColHeight - 1) {
                this.selection += selectionColHeight;
            }
        } else {
            this.selection -= selectionColHeight;
        }
        this.moveSelectionArrow();
    } else if (key == Key.action) {
        if (this.selection < levels.length) {
            currentLevel = this.selection;
            startTransition(1, Transition.seek);
        } else {
            startTransition(0, Transition.seek);
        }
    } else if (key == Key.cancel) {
        startTransition(0, Transition.seek);
    }
}
