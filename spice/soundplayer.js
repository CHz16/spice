// soundplayer.js
// Object for simple playback of sound effects.

function SoundPlayer(soundBuffers, context) {
    this.tick = function(timestamp) {
        
    }

    this.tempoChanged = function(oldTempo) {
        
    }
    
    this.paused = function(pauseTime) {
        this.pauseTime = audioContext.currentTime;
        this.stopAllSounds(true);
    }
    
    this.resumed = function(pausedDuration) {
        for (key in this.startTimes) {
            var elapsedTime = this.pauseTime - this.startTimes[key];
            elapsedTime = elapsedTime % this.soundBuffers[key].duration;
            this.startTimes[key] = audioContext.currentTime - elapsedTime;
            if (elapsedTime < 0) {
                this.play(key, {delay: -elapsedTime, unpausing: true, loop: this.isLooping[key]});
            } else {
                this.play(key, {offset: elapsedTime, unpausing: true, loop: this.isLooping[key]});
            }
        }
        this.pauseTime = undefined;
    }
    
    this.soundBuffers = soundBuffers;
    this.soundSources = new Object();
    this.context = context;
    
    this.startTimes = new Object();
    this.isLooping = new Object();
    this.pauseTime = undefined;
    
    // This is instantiated before the metronome so we need to defer registration
    this.registered = false;
}

SoundPlayer.prototype.play = function(key, args) {
    if (!this.registered) {
        this.registered = true;
        metronome.registerListener(this);
    }
    
    args = args || {};
    delay = args.delay || 0;
    offset = args.offset || 0;
    loop = args.loop || false;
    
    if (key in this.soundSources) {
        this.stop(key);
    }
    
    var source = this.context.createBufferSource();
    this.soundSources[key] = source;
    source.buffer = this.soundBuffers[key];
    if (loop) {
        source.loop = true;
    }
    this.isLooping[key] = loop;
    source.onended = function() { this.stop(key); }.bind(this);
    source.connect(this.context.destination);
    source.start(this.context.currentTime + delay, offset);
    
    if (!args.unpausing) {
        this.startTimes[key] = audioContext.currentTime + delay;
    }
}

SoundPlayer.prototype.stop = function(key, pausing) {
    if (key in this.soundSources) {
        this.soundSources[key].onended = undefined;
        this.soundSources[key].stop();
        this.soundSources[key].disconnect();
        delete this.soundSources[key];
    }
    
    if (!pausing) {
        delete this.startTimes[key];
        delete this.isLooping[key];
    }
}

SoundPlayer.prototype.stopAllSounds = function(pausing) {
    for (key in this.startTimes) {
        this.stop(key, pausing);
    }
}
