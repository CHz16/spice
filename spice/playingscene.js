// playingscene.js
// Controller for the playing scene.

var playingHelpX = 755, playingHelpY = 460, playingHelpLineHeight = 40;
var playingHelpFont = "30px sans-serif";
var playingHealthStartX = 769, playingHealthY = 1, playingHealthWidth = 40;
var playingMistakeGracePeriod = 200; // milliseconds
var hellPeekDuration = 500; // milliseconds

function PlayingSceneController() {
    this.tick = function(timestamp) {
        if (this.hellPeekStopTime <= metronome.currentTime()) {
            this.stopHellPeek();
        }
    }

    this.tempoChanged = function(oldTempo) {
        if (this.isLoading) {
            this.isPlaying = true;
            this.isLoading = false;
            var thirtyseconds = Math.ceil(sceneStartDelay / metronome.duration(Note.thirtysecond));
            var startBeat = metronome.nextBeat(Note.thirtysecond);
            startBeat.beats += thirtyseconds;

            drumMachine.start(startBeat);
            this.currentStep = -1;
            this.takeStep({startBeat: startBeat}, true);

            var introEnd = startBeat.advancedBy(Note.thirtysecond, patternDuration(levels[currentLevel].intro, levels[currentLevel].chartDelay));
            chartManager.start(introEnd);

            animationManager.makeTween({object: this.graphicalChart, property: "scroll", startValue: -1, endValue: 0, startBeat: startBeat, endBeat: introEnd, callback: this.scrollChart.bind(this)});
        }
    }

    this.paused = function(pauseTime) {

    }

    this.resumed = function(pausedDuration) {
        if (this.hellPeekStopTime !== undefined) {
            this.hellPeekStopTime += pausedDuration;
        }
    }

    this.whiffed = function(direction, timestamp) {
        this.spawnArrow(direction, "red", timestamp);
        this.help[0].text = "(You pressed a direction when you shouldn't have!)";
        this.fail();
    }

    this.hitNote = function(note, timestamp) {
        this.spawnArrow(note.direction, "green", timestamp);
        this.correctNotes += 1;
        var i = this.hellPeekSteps.indexOf(this.correctNotes);
        if (i != -1) {
            this.hellPeekSteps.splice(i, 1);
            this.startHellPeek();
        }
    }

    this.missedNote = function(note) {
        this.help[0].text = "(You missed a keypress!)";
        this.fail();
    }

    this.mistookNote = function(note, pressed, timestamp) {
        this.spawnArrow(pressed, "red", timestamp);
        this.help[0].text = "(You pressed the wrong direction!)";
        this.fail();
    }

    this.chartCompleted = function() {
        this.isCelebrating = true;
        drumMachine.stop();
        this.stopHellPeek();
        animationManager.unscheduleAnimationsFor(animationManager.nullAnimationTarget);
        animationManager.unscheduleAnimationsFor(this.objects);
        animationManager.unscheduleAnimationsFor(this.graphicalChart);
        animationManager.unscheduleAnimationsFor(this.playerArrows);

        this.victoryWheel.rotate = 0;
        objectManager.add(this.victoryWheel);

        if (currentLevel == levels.length - 1) {
            objectManager.add(this.pentagram);
            animationManager.unscheduleAnimationsFor(objectManager);
            objectManager.screenShake = 0;
            draw(performance.now(), true);

            // The watching scene never cleared its objects.
            objectManager.remove(scenes[1].objects);
            scenes[1].objects = [];
            scenes[1].namedObjects = {};
            objectManager.remove(scenes[1].name);

            startTransition(3, Transition.immediate);
            currentLevel = undefined;
            return;
        }
        soundPlayer.play("victory");
        objectManager.add(this.victoryText);
        this.spinVictoryWheel();
        currentLevel += 1;
    }

    this.startHellPeek = function() {
        var hellLevel = levels[currentLevel].hellLevel;
        if (levels[currentLevel].progressiveHell) {
            hellLevel *= (levels[currentLevel].hellPeeks - this.hellPeekSteps.length) / levels[currentLevel].hellPeeks;
        }

        soundPlayer.play("hellloop", {loop: true, offset: randomFloatInRange(0, soundPlayer.soundBuffers["hellloop"].duration)});
        this.hellPeekStopTime = metronome.currentTime() + hellPeekDuration * hellLevel;
        objectManager.startHellPeek(hellLevel);
    }

    this.stopHellPeek = function() {
        soundPlayer.stop("hellloop");
        this.hellPeekStopTime = undefined;
        objectManager.stopHellPeek();
    }

    this.spinVictoryWheel = function() {
        animationManager.makeTween({object: this.victoryWheel, property: "rotate", startValue: 0, endValue: 360, startBeat: metronome.currentBeat(Note.whole), endBeat: metronome.nextBeat(Note.whole), callback: this.spinVictoryWheel.bind(this)});
    }

    this.takeStep = function(oldAnim, immediate) {
        var startBeat = oldAnim.startBeat;
        if (!immediate) {
            startBeat = startBeat.advancedBy(levels[currentLevel].steps[this.currentStep].duration);
        }

        if (this.currentStep != -1 && levels[currentLevel].steps[this.currentStep].finish) {
            levels[currentLevel].steps[this.currentStep].finish.bind(this)();
        }
        if (this.currentStep != -1 && levels[currentLevel].steps[this.currentStep].playingFinish) {
            levels[currentLevel].steps[this.currentStep].playingFinish.bind(this)();
        }

        this.currentStep += 1;
        if (this.currentStep == levels[currentLevel].steps.length) {
            this.currentStep = levels[currentLevel].introStepLength;
        }

        var stepData = levels[currentLevel].steps[this.currentStep];
        if (stepData.animations.length == 0) {
            var anim = {startBeat: startBeat, endBeat: startBeat.advancedBy(Note.thirtysecond), callback: this.takeStep.bind(this)};
            animationManager.makeNull(anim);
        } else {
            this.scheduleAnimations(stepData.animations, startBeat, this.takeStep.bind(this));
        }
    }

    this.scheduleAnimations = function(animations, startBeat, callback) {
        for (var i = 0; i < animations.length; i++) {
            var anim = Object.assign({}, animations[i]);
            anim.object = this.namedObjects[anim.object];
            anim.startBeat = startBeat;
            anim.endBeat = startBeat.advancedBy(anim.duration);
            if (i == animations.length - 1) {
                anim.callback = callback;
            }
            if (anim.type == "pulse") {
                animationManager.makePulse(anim);
            } else {
                animationManager.makeTween(anim);
            }
        }
    }

    this.fail = function() {
        var failRoutine = levels[currentLevel].steps[this.currentStep].failRoutine;
        if (failRoutine === undefined) {
            return;
        }

        if (this.lastFailedTime + playingMistakeGracePeriod > metronome.currentTime()) {
            return;
        }
        this.lastFailedTime = metronome.currentTime();
        this.hitPoints -= 1;
        this.health[this.hitPoints].strike = true;
        var now = metronome.rawTime();
        animationManager.makeTween({object: objectManager, property: "screenShake", startValue: 10, endValue: 0, startBeat: now, endBeat: now.rawAdvancedBy(250)});
        if (this.hitPoints > 0) {
            return;
        }

        drumMachine.stop()
        chartManager.stop();
        this.stopHellPeek();
        animationManager.unscheduleAnimationsFor(animationManager.nullAnimationTarget);
        animationManager.unscheduleAnimationsFor(this.objects);
        animationManager.unscheduleAnimationsFor(this.graphicalChart);
        animationManager.unscheduleAnimationsFor(this.playerArrows);

        this.scheduleAnimations(levels[currentLevel].failRoutines[failRoutine], metronome.currentBeat(Note.thirtysecond), function() { });

        this.isPlaying = false;
        this.help[0].x = playingHelpX - this.help[0].calculateWidth(canvasContext);
        objectManager.add(this.help);

        var crashChannels = [0, 1, 5, 6, 7, 8];
        for (var i = 0; i < crashChannels.length; i++) {
            soundPlayer.play(crashChannels[i], {delay: randomFloatInRange(0, 0.2)});
        }
    }

    this.loadData = function(makeObjects) {
        this.isPlaying = false;
        this.isLoading = true;
        this.isResetting = false;
        this.isCelebrating = false;

        this.hitPoints = allowedMistakes + 1;
        for (var i = 0; i < this.hitPoints; i++) {
            this.health[i].strike = false;
        }
        this.lastFailedTime = -99999;

        var levelData = levels[currentLevel];
        drumMachine.loadPattern(levelData.pattern, levelData.intro);

        if (makeObjects) {
            for (var i = 0; i < levelData.objects.length; i++) {
                var spec = levelData.objects[i];
                var args = Object.assign({}, spec.args);
                args.isFree = true;
                var object = new spec.type(args);
                this.objects.push(object);
                this.namedObjects[spec.name] = object;
            }
            objectManager.add(this.objects);
        }

        this.playerArrows = [];
        this.arrowSpawnIndex = 100;

        if (levelData.loops === undefined) {
            levelData.loops = 1;
        }
        chartManager.loadChart(levelData.chart, levelData.loops);

        this.correctNotes = 0;
        this.hellPeekSteps = [];
        if (levelData.hellPeeks === undefined) {
            levelData.hellPeeks = 0;
        }
        while (this.hellPeekSteps.length < levelData.hellPeeks) {
            var s = randomIntInRange(1, levelData.chart.length * levelData.loops - 1);
            if (!this.hellPeekSteps.includes(s)) {
                this.hellPeekSteps.push(s);
            }
        }
    }

    this.scrollChart = function(anim) {
        var scrollStart = (this.graphicalChart.scroll == 0) ? 0 : 1;
        var end = anim.endBeat.advancedBy(Note.thirtysecond, chartDuration(levels[currentLevel].chart));
        animationManager.makeTween({object: this.graphicalChart, property: "scroll", startValue: scrollStart, endValue: scrollStart + 1, startBeat: anim.endBeat, endBeat: end, callback: this.scrollChart.bind(this)});
    }

    this.spawnArrow = function(direction, color, timestamp) {
        var arrow = new ChartArrow({centerX: 100, centerY: 194, color: color, rotate: angleForDirection(direction), zIndex: this.arrowSpawnIndex, isFree: true, fixedDuringHellPeeks: true});
        this.arrowSpawnIndex += 1;
        objectManager.add(arrow);
        this.playerArrows.push(arrow);

        var now = metronome.currentBeat(Note.thirtysecond);
        now.startTime -= now.timestamp() - timestamp;
        now.cachedTimestamp = undefined;

        var hackFactor = 1;
        if (currentLevel == 0) {
            // Level 0's chart is so short that arrows will get culled before
            // they're off screen, so we'll extend the distance they go.
            hackFactor = 2;
        }

        animationManager.makeTween({object: arrow, property: "centerY", endIncrement: -this.graphicalChart.canvasHeight * hackFactor, startBeat: now, endBeat: now.advancedBy(Note.thirtysecond, this.chartLength * hackFactor), callback: this.removeArrow.bind(this)}, metronome.currentTime());
    }

    this.removeArrow = function(anim) {
        var i = this.playerArrows.indexOf(anim.object);
        this.playerArrows.splice(i, 1);
        objectManager.remove(anim.object);
    }


    this.wantsDiagonals = true;

    this.isPlaying = false, this.isLoading = false, this.isResetting = false, this.isCelebrating = false;
    this.objects = [], this.namedObjects = {};
    this.currentStep = 0;
    this.correctNotes = 0, this.hellPeekSteps = [], this.hellPeekStopTime = undefined;

    this.hitPoints = undefined, this.lastFailedTime = undefined;
    this.health = undefined;

    this.graphicalChart = undefined;
    this.chartLength = undefined;
    this.playerArrows = undefined;
    this.arrowSpawnIndex = undefined;

    this.help = [
        new Label({x: playingHelpX, y: playingHelpY, font: playingHelpFont, text: "", isFree: true, zIndex: 100}),
        new Label({x: playingHelpX, y: playingHelpY + playingHelpLineHeight, font: playingHelpFont, text: "[Z]: Retry", isFree: true, zIndex: 100}),
        new Label({x: playingHelpX, y: playingHelpY + playingHelpLineHeight * 2, font: playingHelpFont, text: "[X]: Replay the video", isFree: true, zIndex: 100})
    ];
    for (var i = 1; i < this.help.length; i++) {
        this.help[i].x = playingHelpX - this.help[i].calculateWidth(canvasContext);
    }
    this.victoryText = new Label({y: playingHelpY + playingHelpLineHeight * 2, font: playingHelpFont, text: "[X]: Try the next sex", isFree: true, zIndex: 100});
    this.victoryText.x = playingHelpX - this.victoryText.calculateWidth(canvasContext);
    this.victoryWheel = new VictoryWheel({centerX: gameWidth / 2, centerY: gameHeight / 2, radius: Math.sqrt(gameWidth * gameWidth + gameHeight * gameHeight) / 2 + 1, alpha: 0.2, isFree: true, zIndex: -100});
    this.pentagram = new Pentagram({centerX: gameWidth / 2, centerY: gameHeight / 2, radius: 200, rotate: 36, level: 1, isFree: true, zIndex: -99});
}

PlayingSceneController.prototype.draw = function(timestamp) {

}

PlayingSceneController.prototype.startTransitionIn = function(transitionType) {
    this.health = [];
    for (var i = 0; i < allowedMistakes + 1; i++) {
        this.health.push(new Health({x: playingHealthStartX - i * playingHealthWidth, y: playingHealthY, isFree: true, fixedDuringHellPeeks: true, zIndex: 100}));
    }
    objectManager.add(this.health);

    this.loadData(true);

    var levelData = levels[currentLevel];
    this.graphicalChart = new GraphicalChart({x: 50, y: 50, height: 500, zIndex: 100, isFree: true, fixedDuringHellPeeks: true, scroll: -1, chart: levelData.chart, chartDelay: levelData.chartDelay, intro: levelData.intro, barLength: levelData.barLength});
    this.chartLength = chartDuration(levelData.chart);
    objectManager.add(this.graphicalChart);
}

PlayingSceneController.prototype.finishTransitionIn = function(transitionType) {
    metronome.registerListener(this);
    metronome.setTempo(levels[currentLevel].tempo);
    chartManager.registerListener(this);
    // Starting the round finishes in the .tempoChanged handler, which will
    // be triggered because we just changed the tempo.
}

PlayingSceneController.prototype.startTransitionOut = function(transitionType) {
    metronome.unregisterListener(this);
    drumMachine.stop();
    chartManager.stop();
    chartManager.unregisterListener(this);
    this.stopHellPeek();
    soundPlayer.stopAllSounds();
    animationManager.unscheduleAnimationsFor(animationManager.nullAnimationTarget);
    animationManager.unscheduleAnimationsFor(this.objects);
    animationManager.unscheduleAnimationsFor(this.graphicalChart);
    animationManager.unscheduleAnimationsFor(this.playerArrows);
    animationManager.unscheduleAnimationsFor(this.victoryWheel);
}

PlayingSceneController.prototype.finishTransitionOut = function(transitionType) {
    objectManager.remove(this.objects);
    this.objects = [];
    this.namedObjects = {};
    objectManager.remove(this.health);
    objectManager.remove(this.graphicalChart);
    objectManager.remove(this.playerArrows);
    objectManager.remove(this.help);
    objectManager.remove(this.victoryText);
    objectManager.remove(this.victoryWheel);
    objectManager.remove(this.pentagram);
}

PlayingSceneController.prototype.keyDown = function(key, timestamp) {
    if (key == Key.cancel) {
        startTransition(1, Transition.zoomIn);
    } else if (key == Key.action) {
        if (this.isCelebrating) {
            startTransition(1, Transition.zoomIn);
        } else if (!this.isResetting) {
            this.isResetting = true;
            this.startTransitionOut(Transition.immediate);

            objectManager.remove(this.playerArrows);
            objectManager.remove(this.help);
            var startBeat = metronome.nextBeat(Note.thirtysecond);
            animationManager.makeTween({object: this.graphicalChart, property: "scroll", startBeat: startBeat, endBeat: startBeat.advancedBy(Note.sixteenth), endValue: -1});

            this.scheduleAnimations(levels[currentLevel].reset, startBeat, function() { this.loadData(false); this.finishTransitionIn(Transition.immediate); }.bind(this));
        }
    } else {
        chartManager.signal(key, timestamp);
    }
}
