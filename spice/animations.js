// animations.js
// Global animation manager object with animation creators.

function AnimationManager() {
    this.tick = function(timestamp) {
        if (this.pauseTime) {
            return;
        }
        
        var i = 0;
        while (i < this.activeAnimations.length) {
            if (this.updateAnimation(i, timestamp)) {
                i += 1;
            }
        }
    }

    this.tempoChanged = function(oldTempo) {
        for (var i = 0; i < this.activeAnimations.length; i++) {
            var anim = this.activeAnimations[i];
            anim.startBeat = anim.startBeat.reinterpretWithCurrentMetronome();
            anim.endBeat = anim.endBeat.reinterpretWithCurrentMetronome();
        }
    }
    
    this.paused = function(pauseTime) {
        this.pauseTime = pauseTime;
    }
    
    this.resumed = function(pausedDuration) {
        for (var i = 0; i < this.activeAnimations.length; i++) {
            this.activeAnimations[i].startBeat.startTime += pausedDuration;
            this.activeAnimations[i].startBeat.cachedTimestamp = undefined;
            this.activeAnimations[i].endBeat.startTime += pausedDuration;
            this.activeAnimations[i].endBeat.cachedTimestamp = undefined;
        }
        this.pauseTime = undefined;
    }
    
    this.updateAnimation = function(i, timestamp) {
        var anim = this.activeAnimations[i];
        if (timestamp >= anim.endBeat.timestamp()) {
            anim.object.isAnimating = false;
            this.activeAnimations.splice(i, 1);
            anim.object[anim.property] = anim.endValue;
            anim.callback(anim);
            return false;
        } else if (timestamp >= anim.startBeat.timestamp()) {
            anim.object.isAnimating = true;
            var t = timestamp - anim.startBeat.timestamp();
            var b = anim.startValue;
            var c = anim.endValue - anim.startValue;
            var d = anim.endBeat.timestamp() - anim.startBeat.timestamp();
            anim.object[anim.property] = anim.easing(t, b, c, d);
            return true;
        } else {
            return true;
        }
    }

    this.nullAnimationTarget = {};
    this.activeAnimations = [];
    this.pauseTime = undefined;
    metronome.registerListener(this);
}

//
// MANAGEMENT
//

AnimationManager.prototype.unscheduleAnimationsFor = function(object) {
    if (Array.isArray(object)) {
        for (var i = 0; i < object.length; i++) {
            this.unscheduleAnimationsFor(object[i]);
        }
        return;
    }
    
    var i = 0;
    while (i < this.activeAnimations.length) {
        if (this.activeAnimations[i].object == object) {
            this.activeAnimations.splice(i, 1);
        } else {
            i += 1;
        }
    }
}

//
// CREATION
//

// object, property
// startBeat, endBeat
// startValue (optional), endValue or endIncrement
// easing (optional)
// callback (optional)
AnimationManager.prototype.makeTween = function(args, timestamp) {
    args.type = "tween";
    if (args.startValue === undefined) {
        args.startValue = args.object[args.property];
    }
    if (args.endIncrement !== undefined) {
        args.endValue = args.startValue + args.endIncrement;
    }
    args.easing = args.easing || linearTween;
    args.callback = args.callback || function() { };
    
    // Clone so it can be used for other animations without smashing
    args.startBeat = args.startBeat.copy();
    args.endBeat = args.endBeat.copy();
    
    this.activeAnimations.push(args);
    
    if (this.pauseTime) {
        timestamp = this.pauseTime;
    }
    this.updateAnimation(this.activeAnimations.length - 1, timestamp);
}

// object, property
// startBeat, endBeat
// pulseFactor or pulseDisplacement (optional)
// callback (optional)
AnimationManager.prototype.makePulse = function(args, timestamp) {
    args.type = "tween";
    args.startValue = args.object[args.property];
    args.endValue = args.object[args.property];
    
    if (!args.easing) {
        var stretch;
        if (args.pulseDisplacement !== undefined) {
            stretch = args.pulseDisplacement;
        } else {
            args.pulseFactor = args.pulseFactor || 1.1;
            stretch = (args.pulseFactor - 1) * args.startValue;
        }
        args.easing = function(t, b, c, d) {
            return b + stretch * (1 - 2 * Math.abs(.5 - t / d));
        }
    } else {
        args.easing = args.easing.bind(args);
    }
    
    args.callback = args.callback || function() { };
    
    // Clone so it can be used for other animations without smashing
    args.startBeat = args.startBeat.copy();
    args.endBeat = args.endBeat.copy();
    
    this.activeAnimations.push(args);
    this.updateAnimation(this.activeAnimations.length - 1, timestamp);
}

// object, property
// startBeat, endBeat
// startValue (optional)
// callback (optional)
AnimationManager.prototype.makeGravity = function(args) {
    args.type = "tween";
    if (args.startValue === undefined) {
        args.startValue = args.object[args.property];
    }
    args.easing = function(t, b, c, d) {
        var gravity = 10;
        t /= 150;
        return b + gravity * t * t;
    }
    var duration = args.endBeat.timestamp() - args.startBeat.timestamp();
    args.endValue = args.easing(duration, args.startValue, 0, duration);
    args.callback = args.callback || function() { };
    
    // Clone so it can be used for other animations without smashing
    args.startBeat = args.startBeat.copy();
    args.endBeat = args.endBeat.copy();
    
    this.activeAnimations.push(args);
}

AnimationManager.prototype.makeNull = function(args) {
    args.type = "tween";
    args.object = this.nullAnimationTarget;
    args.property = "asdf";
    args.startValue = 0;
    args.endValue = 0;
    args.easing = function() { return 0; }
    args.callback = args.callback || function() { };
    
    // Clone so it can be used for other animations without smashing
    if (args.startBeat) {
        args.startBeat = args.startBeat.copy();
    } else {
        args.startBeat = new RawTimestamp(0);
    }
    args.endBeat = args.endBeat.copy();
    
    this.activeAnimations.push(args);
}


function linearTween(t, b, c, d) {
    return c * t / d + b;
}

function easeInQuad(t, b, c, d) {
    t /= d;
    return c * t * t + b;
}

function easeOutQuad(t, b, c, d) {
    t /= d;
    return -c * t * (t - 2) + b;
}
