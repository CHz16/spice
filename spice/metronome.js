// metronome.js
// Global metronome object.

var Note = {whole: 0, half: 1, quarter: 2, eighth: 3, sixteenth: 4, thirtysecond: 5};

function Metronome(tempo) {
    this.currentTime = function() {
        return performance.now();
    }
    
    this.calculateDurations = function() {
        var quarterDuration = 60 * 1000 / this.tempo;
        this.durations = [quarterDuration * 4, quarterDuration * 2, quarterDuration, quarterDuration / 2, quarterDuration / 4, quarterDuration / 8];
    }
    
    this.listeners = [];
    
    this.isRunning = false;
    this.futureTempo = undefined;
    this.futureTempoChangeTime = undefined, this.futureTempoStartTime = undefined;
    
    this.startTime = undefined;
    this.pauseTime = undefined;
    this.tempo = tempo || 120;
    this.calculateDurations();
}

function Beat(startTime, thirtysecondDuration, beats) {
    this.startTime = startTime;
    this.thirtysecondDuration = thirtysecondDuration;
    this.beats = beats;
}

Beat.prototype.copy = function() {
    return new Beat(this.startTime, this.thirtysecondDuration, this.beats);
}

Beat.prototype.advancedBy = function(note, beats) {
    if (beats === undefined) {
        beats = 1;
    }
    
    if (Array.isArray(note)) {
        var newBeat = this.copy();
        for (var i = 0; i < note.length; i++) {
            newBeat = newBeat.advancedBy(note[i], beats);
        }
        return newBeat;
    }
    
    var multiplier = 1;
    switch(note) {
        case Note.whole:
            multiplier = 32;
            break;
        case Note.half:
            multiplier = 16;
            break;
        case Note.quarter:
            multiplier = 8;
            break;
        case Note.eighth:
            multiplier = 4;
            break;
        case Note.sixteenth:
            multiplier = 2;
            break;
    }
    return new Beat(this.startTime, this.thirtysecondDuration, this.beats + multiplier * beats);
}

Beat.prototype.timestamp = function() {
    if (!this.cachedTimestamp) {
        this.cachedTimestamp = this.startTime + this.thirtysecondDuration * this.beats;
    }
    return this.cachedTimestamp;
}

Beat.prototype.reinterpretWithCurrentMetronome = function() {
    var remainingThirtyseconds = (this.timestamp() - metronome.currentTime()) / this.thirtysecondDuration;
    return new Beat(metronome.currentTime(), metronome.duration(Note.thirtysecond), remainingThirtyseconds);
}

Beat.prototype.rawTimeFromNow = function() {
    return this.timestamp() - metronome.currentTime();
}

function RawTimestamp(time) {
    this.startTime = time;
}

RawTimestamp.prototype.copy = function() {
    return new RawTimestamp(this.startTime);
}

RawTimestamp.prototype.advancedBy = function(note, beats) {
    if (Array.isArray(note)) {
        var newRawTimestamp = this.copy();
        for (var i = 0; i < note.length; i++) {
            newRawTimestamp = newRawTimestamp.advancedBy(note[i], beats);
        }
        return newRawTimestamp;
    }
    return new RawTimestamp(this.startTime + metronome.duration(note) * beats);
}

RawTimestamp.prototype.rawAdvancedBy = function(offset) {
    return new RawTimestamp(this.startTime + offset);
}

RawTimestamp.prototype.timestamp = function() {
    return this.startTime;
}

RawTimestamp.prototype.reinterpretWithCurrentMetronome = function() {
    return this.copy();
}

RawTimestamp.prototype.rawTimeFromNow = function() {
    return this.timestamp() - metronome.currentTime();
}

//
// MANAGEMENT
//

Metronome.prototype.tick = function() {
    if (this.pauseTime) {
        return;
    }
    
    var timestamp = this.currentTime();
    if (this.futureTempoChangeTime && timestamp >= this.futureTempoChangeTime) {
        var oldTempo = this.tempo;
        this.tempo = this.futureTempo;
        this.startTime = this.futureTempoStartTime;
        this.calculateDurations();
        for (var i = 0; i < this.listeners.length; i++) {
            this.listeners[i].tempoChanged(oldTempo);
        }
                
        this.futureTempo = undefined;
        this.futureTempoChangeTime = undefined;
        this.futureTempoStartTime = undefined;
    }
    
    for (var i = 0; i < this.listeners.length; i++) {
        this.listeners[i].tick(timestamp);
    }
}

Metronome.prototype.setTempo = function(tempo) {
    this.futureTempo = tempo;
    this.futureTempoChangeTime = this.nextBeat(Note.thirtysecond).timestamp();
    var thirtyseconds = Math.round((this.futureTempoChangeTime - this.currentBeat(Note.whole).timestamp()) / this.duration(Note.thirtysecond)) % 32;
    this.futureTempoStartTime = this.futureTempoChangeTime - thirtyseconds * ((60 / 8) * 1000 / this.futureTempo);
}

Metronome.prototype.start = function(timestamp) {
    this.startTime = timestamp;
    this.pauseTime = undefined;
    
    if (this.futureTempo) {
        var oldTempo = this.tempo;
        
        this.tempo = this.futureTempo;
        this.calculateDurations();
        
        this.futureTempo = undefined;
        this.futureTempoChangeTime = undefined;
        this.futureTempoStartTime = undefined;
        
        for (var i = 0; i < this.listeners.length; i++) {
            this.listeners[i].tempoChanged(oldTempo);
        }
    }
}

Metronome.prototype.pause = function() {
    this.pauseTime = this.currentTime();
    for (var i = 0; i < this.listeners.length; i++) {
        this.listeners[i].paused(this.pauseTime);
    }
}

Metronome.prototype.resume = function() {
    var pausedDuration = this.currentTime() - this.pauseTime;
    this.startTime += pausedDuration;
    if (this.futureTempoChangeTime) {
        this.futureTempoChangeTime += pausedDuration;
        this.futureTempoStartTime += pausedDuration;
    }
    this.pauseTime = undefined;
    for (var i = 0; i < this.listeners.length; i++) {
        this.listeners[i].resumed(pausedDuration);
    }
}

Metronome.prototype.registerListener = function(listener) {
    if (!this.listeners.includes(listener)) {
        this.listeners.push(listener);
    }
}

Metronome.prototype.unregisterListener = function(listener) {
    var i = this.listeners.indexOf(listener);
    if (i != -1) {
        this.listeners.splice(i, 1);
    }
}

//
// SCHEDULING
//

Metronome.prototype.currentBeat = function(note) {
    return this.beat(note, 0);
}

Metronome.prototype.nextBeat = function(note) {
    return this.beat(note, 1);
}

Metronome.prototype.beat = function(note, offset) {
    var time = this.pauseTime || this.currentTime();
    var beatDuration = this.duration(note);
    var thirtysecondDuration = this.duration(Note.thirtysecond);
    var beatsSinceStart = Math.floor((time - this.startTime) / beatDuration);
    return new Beat(this.startTime, thirtysecondDuration, Math.round((beatsSinceStart + offset) * (beatDuration / thirtysecondDuration)));
}

Metronome.prototype.duration = function(note) {
    return this.durations[note];
}

Metronome.prototype.rawTime = function(offset) {
    offset = offset || 0;
    return new RawTimestamp(this.currentTime() + offset);
}
