// watchingscene.js
// Controller for the DVD watching scene.

var watchingNameX = 755, watchingNameY = 75;
var watchingHelpX = 45, watchingHelpY = 500, watchingHelpLineHeight = 40;
var watchingHelpFont = "30px sans-serif";

function WatchingSceneController() {
    this.tick = function(timestamp) {

    }

    this.tempoChanged = function(oldTempo) {
        if (!this.isPlaying) {
            this.isPlaying = true;
            var thirtyseconds = Math.ceil(sceneStartDelay / metronome.duration(Note.thirtysecond));
            var startBeat = metronome.nextBeat(Note.thirtysecond);
            startBeat.beats += thirtyseconds;

            drumMachine.start(startBeat);
            this.currentStep = -1;
            this.takeStep({startBeat: startBeat}, true);

            var introEnd = startBeat.advancedBy(Note.thirtysecond, patternDuration(levels[currentLevel].intro, levels[currentLevel].chartDelay));
            chartManager.start(introEnd);

            animationManager.makeTween({object: this.graphicalChart, property: "scroll", startValue: -1, endValue: 0, startBeat: startBeat, endBeat: introEnd, callback: this.scrollChart.bind(this)});
        }
    }

    this.paused = function(pauseTime) {

    }

    this.resumed = function(pausedDuration) {

    }

    this.whiffed = function(direction, timestamp) {
        this.spawnArrow(direction, "red", timestamp);
    }

    this.hitNote = function(note, timestamp) {
        this.spawnArrow(note.direction, "green", timestamp);
    }

    this.missedNote = function(note) {

    }

    this.mistookNote = function(note, pressed, timestamp) {
        this.spawnArrow(pressed, "red", timestamp);
    }

    this.chartCompleted = function() {

    }

    this.takeStep = function(oldAnim, immediate) {
        var startBeat = oldAnim.startBeat;
        if (!immediate) {
            startBeat = startBeat.advancedBy(levels[currentLevel].steps[this.currentStep].duration);
        }

        if (this.currentStep != -1 && (!levels[currentLevel].onlyFinishWhenPracticing || this.startedPracticing) && levels[currentLevel].steps[this.currentStep].finish) {
            levels[currentLevel].steps[this.currentStep].finish.bind(this)();
        }

        this.currentStep += 1;
        if (this.currentStep == levels[currentLevel].steps.length) {
            this.currentStep = levels[currentLevel].introStepLength;
        }

        var stepData = levels[currentLevel].steps[this.currentStep];
        if (stepData.animations.length == 0) {
            var anim = {startBeat: startBeat, endBeat: startBeat.advancedBy(Note.thirtysecond), callback: this.takeStep.bind(this)};
            animationManager.makeNull(anim);
        } else {
            for (var i = 0; i < stepData.animations.length; i++) {
                var anim = Object.assign({}, stepData.animations[i]);
                anim.object = this.namedObjects[anim.object];
                anim.startBeat = startBeat;
                anim.endBeat = startBeat.advancedBy(anim.duration);
                if (i == stepData.animations.length - 1) {
                    anim.callback = this.takeStep.bind(this);
                }
                if (anim.type == "pulse") {
                    animationManager.makePulse(anim);
                } else {
                    animationManager.makeTween(anim);
                }
            }
        }
    }

    this.scrollChart = function(anim) {
        var scrollStart = (this.graphicalChart.scroll == 0) ? 0 : 1;
        var end = anim.endBeat.advancedBy(Note.thirtysecond, chartDuration(levels[currentLevel].chart));
        animationManager.makeTween({object: this.graphicalChart, property: "scroll", startValue: scrollStart, endValue: scrollStart + 1, startBeat: anim.endBeat, endBeat: end, callback: this.scrollChart.bind(this)});
    }

    this.spawnArrow = function(direction, color, timestamp) {
        var arrow = new ChartArrow({centerX: 100, centerY: 194, color: color, rotate: angleForDirection(direction), zIndex: this.arrowSpawnIndex});
        this.arrowSpawnIndex += 1;
        objectManager.add(arrow);
        this.playerArrows.push(arrow);

        var now = metronome.currentBeat(Note.thirtysecond);
        now.startTime -= now.timestamp() - timestamp;
        now.cachedTimestamp = undefined;

        var hackFactor = 1;
        if (currentLevel == 0) {
            // Level 0's chart is so short that arrows will get culled before
            // they're off screen, so we'll extend the distance they go.
            hackFactor = 2;
        }

        animationManager.makeTween({object: arrow, property: "centerY", endIncrement: -this.graphicalChart.canvasHeight * hackFactor, startBeat: now, endBeat: now.advancedBy(Note.thirtysecond, this.chartLength * hackFactor), callback: this.removeArrow.bind(this)}, metronome.currentTime());
    }

    this.removeArrow = function(anim) {
        var i = this.playerArrows.indexOf(anim.object);
        this.playerArrows.splice(i, 1);
        objectManager.remove(anim.object);
    }


    this.wantsDiagonals = true;

    this.isPlaying = false;
    this.objects = [], this.namedObjects = {};
    this.currentStep = 0;
    this.name = new Label({y: watchingNameY, font: watchingHelpFont, zIndex: 100});

    this.startedPracticing = false;
    this.graphicalChart = undefined;
    this.chartLength = undefined;
    this.playerArrows = undefined;
    this.arrowSpawnIndex = undefined;
    this.practiceLabel = new Label({x: 88, y: 200, text: "(Practice copying the arrow's movements in the video.)", font: "20px sans-serif", zIndex: 100});

    this.help = [
        new Label({y: watchingHelpY, font: watchingHelpFont, text: "[Z]: Get dirty", zIndex: 100}),
        new Label({y: watchingHelpY + watchingHelpLineHeight, font: watchingHelpFont, text: "[X]: Return to the main menu", zIndex: 100})
    ];
    for (var i = 0; i < this.help.length; i++) {
        this.help[i].x = watchingNameX - this.help[i].calculateWidth(canvasContext);
    }
}

WatchingSceneController.prototype.draw = function(timestamp) {

}

WatchingSceneController.prototype.startTransitionIn = function(transitionType) {
    if (currentLevel === undefined) {
        currentLevel = 0;
    }

    this.isPlaying = false;

    var levelData = levels[currentLevel];
    drumMachine.loadPattern(levelData.pattern, levelData.intro);

    if (transitionType == Transition.zoomIn) {
        objectManager.remove(this.objects);
        this.objects = [];
        this.namedObjects = {};
        objectManager.remove(this.name);
        objectManager.remove(this.graphicalChart);
    }
    for (var i = 0; i < levelData.objects.length; i++) {
        var spec = levelData.objects[i];
        var object = new spec.type(spec.args);
        this.objects.push(object);
        this.namedObjects[spec.name] = object;
    }

    this.name.text = levelData.name;
    this.name.x = watchingNameX - this.name.calculateWidth(canvasContext);

    this.graphicalChart = new GraphicalChart({x: 50, y: 50, height: 500, zIndex: 100, scroll: -1, chart: levelData.chart, chartDelay: levelData.chartDelay, intro: levelData.intro, barLength: levelData.barLength});
    this.chartLength = chartDuration(levelData.chart);
    this.playerArrows = [];
    this.arrowSpawnIndex = 100;

    if (transitionType == Transition.zoomIn) {
        objectManager.add(this.objects);
        objectManager.add(this.help);
        objectManager.add(this.name);
        objectManager.add(this.graphicalChart);

        if (currentLevel == 0) {
            objectManager.add(this.practiceLabel);
        }
    }

    chartManager.loadChart(levelData.chart, "forever");

    this.startedPracticing = false;
}

WatchingSceneController.prototype.finishTransitionIn = function(transitionType) {
    if (transitionType == Transition.seek || transitionType == Transition.immediate) {
        objectManager.add(this.objects);
        objectManager.add(this.help);
        objectManager.add(this.name);
        objectManager.add(this.graphicalChart);

        if (currentLevel == 0) {
            objectManager.add(this.practiceLabel);
        }
    }

    metronome.registerListener(this);
    metronome.setTempo(levels[currentLevel].tempo);
    chartManager.registerListener(this);
    // Starting the round finishes in the .tempoChanged handler, which will
    // be triggered because we just changed the tempo.
}

WatchingSceneController.prototype.startTransitionOut = function(transitionType) {
    metronome.unregisterListener(this);
    chartManager.unregisterListener(this);
    chartManager.stop();
    drumMachine.stop();
    animationManager.unscheduleAnimationsFor(animationManager.nullAnimationTarget);
    animationManager.unscheduleAnimationsFor(this.objects);
    animationManager.unscheduleAnimationsFor(this.graphicalChart);
    animationManager.unscheduleAnimationsFor(this.playerArrows);

    if (transitionType == Transition.seek || transitionType == Transition.immediate) {
        objectManager.remove(this.help);
        objectManager.remove(this.practiceLabel);
        objectManager.remove(this.playerArrows);
    }
}

WatchingSceneController.prototype.finishTransitionOut = function(transitionType) {
    if (transitionType == Transition.seek || transitionType == Transition.immediate) {
        objectManager.remove(this.objects);
        this.objects = [];
        this.namedObjects = {};
        objectManager.remove(this.name);
        objectManager.remove(this.graphicalChart);
    } else {
        objectManager.remove(this.help);
        objectManager.remove(this.practiceLabel);
        objectManager.remove(this.playerArrows);
    }
}

WatchingSceneController.prototype.keyDown = function(key, timestamp) {
    if (key == Key.cancel) {
        startTransition(0, Transition.seek);
    } else if (key == Key.action) {
        startTransition(2, Transition.zoomOut);
    } else {
        if (!this.startedPracticing) {
            this.startedPracticing = true;
            objectManager.remove(this.practiceLabel);
        }
        chartManager.signal(key, timestamp);
    }
}
