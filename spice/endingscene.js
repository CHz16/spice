// endingscene.js
// Controller for the ending scene.

var screenShakeIntensity = 10;

function EndingSceneController() {
    this.takeStep = function() {
        if (this.currentStep == this.steps.length) {
            startTransition(4, Transition.immediate);
            return;
        }

        this.steps[this.currentStep]();
        this.currentStep += 1;
    }

    this.crash1 = function() {
        soundPlayer.play("8");

        objectManager.add(this.flash);
        var now = metronome.rawTime();
        animationManager.makeTween({object: this.flash, property: "alpha", startValue: 1, endValue: 0, startBeat: now, endBeat: now.rawAdvancedBy(1000)});
        animationManager.makeNull({endBeat: now.rawAdvancedBy(2000), callback: this.takeStep.bind(this)});
    }

    this.crash2 = function() {
        soundPlayer.play("8");
        this.moveShards(1);

        var now = metronome.rawTime();
        animationManager.makeTween({object: this.flash, property: "alpha", startValue: 1, endValue: 0, startBeat: now, endBeat: now.rawAdvancedBy(1000)});
        animationManager.makeNull({endBeat: now.rawAdvancedBy(300), callback: this.takeStep.bind(this)});
    }

    this.crash3 = function() {
        soundPlayer.play("8");
        this.moveShards(2);

        var now = metronome.rawTime();
        animationManager.unscheduleAnimationsFor(this.flash);
        animationManager.makeTween({object: this.flash, property: "alpha", startValue: 1, endValue: 0, startBeat: now, endBeat: now.rawAdvancedBy(1000)});
        animationManager.makeNull({endBeat: metronome.rawTime(2000), callback: this.takeStep.bind(this)});
    }

    this.crash4 = function() {
        soundPlayer.play("8");
        this.moveShards(5);
        this.flickerBackground();

        var now = metronome.rawTime();
        animationManager.makeTween({object: this.flash, property: "alpha", startValue: 1, endValue: 0, startBeat: now, endBeat: now.rawAdvancedBy(1000)});

        var end = now.rawAdvancedBy(2000);
        for (var i = 0; i < this.shards.length; i++) {
            animationManager.makeTween({object: this.shards[i], property: "x", endIncrement: randomIntInRange(-100, 100), startBeat: now, endBeat: end});
            animationManager.makeTween({object: this.shards[i], property: "rotate", endIncrement: randomFloatInRange(-30, 30), startBeat: now, endBeat: end});
            animationManager.makeGravity({object: this.shards[i], property: "y", startBeat: now, endBeat: end});
        }
    }

    this.moveShards = function(range) {
        for (var i = 0; i < this.shards.length; i++) {
            this.shards[i].x += randomIntInRange(-range, range);
            this.shards[i].y += randomIntInRange(-range, range);
            this.shards[i].rotate += randomFloatInRange(-range, range);
        }
    }

    this.flickerBackground = function() {
        this.rFlickerMax += 5;
        if (this.rFlickerMax >= 150) {
            this.gbFlickerMax += 5;
        }
        var r = clamp(randomIntInRange(this.rFlickerMax - 75, this.rFlickerMax), 0, 255);
        var gb = clamp(randomIntInRange(this.gbFlickerMax - 75, this.gbFlickerMax), 0, 255);
        this.background.color = "rgb(" + r + ", " + gb + ", " + gb + ")";

        if (this.gbFlickerMax <= 400) {
            animationManager.makeNull({endBeat: metronome.rawTime(randomIntInRange(50, 150)), callback: this.flickerBackground.bind(this)});
        } else {
            this.takeStep();
        }
    }

    this.suburbs1 = function() {
        objectManager.remove(this.flash);
        objectManager.remove(this.shards);
        objectManager.remove(this.background);
        soundPlayer.stop("hellloop");

        objectManager.add(this.suburbs);
        this.house.x = 432;
        this.house.y = 242;
        this.house.rotate = 6;
        objectManager.add(this.house);
        soundPlayer.play("menu");

        animationManager.makeNull({endBeat: metronome.rawTime(3250), callback: this.takeStep.bind(this)});
    }

    this.suburbs2 = function() {
        this.house.rotate += 5;
        animationManager.makeNull({endBeat: metronome.rawTime(1980), callback: this.takeStep.bind(this)});
    }

    this.suburbs3 = function() {
        this.house.x += 2;
        this.house.y += 2
        this.house.rotate -= 10;
        animationManager.makeNull({endBeat: metronome.rawTime(1370), callback: this.takeStep.bind(this)});
    }

    this.suburbs4 = function() {
        this.house.x -= 3;
        this.house.y -= 1;
        this.house.rotate -= 8;
        animationManager.makeNull({endBeat: metronome.rawTime(1200), callback: this.takeStep.bind(this)});
    }

    this.suburbs5 = function() {
        this.house.x += 1;
        this.house.y -= 4;
        this.house.rotate -= 4;
        animationManager.makeNull({endBeat: metronome.rawTime(300), callback: this.takeStep.bind(this)});
    }

    this.suburbs6 = function() {
        this.house.x += 4;
        this.house.y += 2;
        this.house.rotate += 216;
        animationManager.makeNull({endBeat: metronome.rawTime(400), callback: this.takeStep.bind(this)});
    }

    this.suburbs7 = function() {
        this.house.x -= 3;
        this.house.y += 3;
        this.house.rotate -= 211;
        animationManager.makeNull({endBeat: metronome.rawTime(830), callback: this.takeStep.bind(this)});
    }

    this.suburbs8 = function() {
        this.house.x -= 4;
        this.house.y -= 4;
        this.house.rotate += 7;
        animationManager.makeNull({endBeat: metronome.rawTime(500), callback: this.takeStep.bind(this)});
    }

    this.explode = function() {
        soundPlayer.stop("menu");
        soundPlayer.play("8");
        this.isClamoring = true;
        this.clamor();

        this.isFlickeringFissure = true;
        objectManager.add(this.fissure);
        this.flickerFissure();
        this.plume.alpha = 1;
        this.plume.waveOffset = 0;
        objectManager.add(this.plume);

        var now = metronome.rawTime();
        var end = metronome.rawTime(1000);
        animationManager.makeTween({object: this.plume, property: "waveOffset", endValue: -200, startBeat: now, endBeat: end});
        animationManager.makeTween({object: this.plume, property: "alpha", endValue: 0, startBeat: now, endBeat: end});

        animationManager.makeTween({object: this.house, property: "x", endValue: 480, startBeat: now, endBeat: end});
        animationManager.makeTween({object: this.house, property: "y", endValue: -200, startBeat: now, endBeat: end});
        animationManager.makeTween({object: this.house, property: "rotate", endIncrement: 1000, startBeat: now, endBeat: end});

        objectManager.add(this.flash);
        animationManager.makeTween({object: this.flash, property: "alpha", startValue: 1, endValue: 0, startBeat: now, endBeat: end});

        objectManager.screenShake = screenShakeIntensity;
        animationManager.makeTween({object: objectManager, property: "screenShake", endValue: 0, startBeat: now, endBeat: end});

        animationManager.makeNull({endBeat: metronome.rawTime(1500), callback: this.takeStep.bind(this)});
    }

    this.flickerFissure = function() {
        if (!this.isFlickeringFissure) {
            return;
        }

        this.fissure.flicker();
        animationManager.makeNull({endBeat: metronome.rawTime(randomIntInRange(50, 150)), callback: this.flickerFissure.bind(this)});
    }

    this.clamor = function() {
        if (!this.isClamoring) {
            return;
        }

        var clamorSounds = ["0", "1", "3", "4", "5", "6", "7", "8", "9", "10", "11"];
        soundPlayer.play(clamorSounds[randomIntInRange(0, clamorSounds.length - 1)]);

        animationManager.makeNull({endBeat: metronome.rawTime(randomIntInRange(50, 150)), callback: this.clamor.bind(this)});
    }

    this.finishExplode = function() {
        this.isClamoring = false;
        objectManager.remove(this.flash);
        objectManager.remove(this.house);
        objectManager.remove(this.plume);

        animationManager.makeNull({endBeat: metronome.rawTime(4000), callback: this.takeStep.bind(this)});
    }

    this.fadeOut1 = function() {
        objectManager.add(this.fade1);
        this.fade1.alpha = 0;
        var now = metronome.rawTime();
        animationManager.makeTween({object: this.fade1, property: "alpha", startValue: 0, endValue: 1, startBeat: now, endBeat: now.rawAdvancedBy(1500), callback: this.takeStep.bind(this)});
    }

    this.fadeOut2 = function() {
        objectManager.remove(this.suburbs);
        animationManager.makeNull({endBeat: metronome.rawTime(2500), callback: this.takeStep.bind(this)});
    }

    this.fadeOut3 = function() {
        objectManager.add(this.fade2);
        this.fade2.alpha = 0;
        var now = metronome.rawTime();
        animationManager.makeTween({object: this.fade2, property: "alpha", startValue: 0, endValue: 1, startBeat: now, endBeat: now.rawAdvancedBy(1000), callback: this.takeStep.bind(this)});
    }

    this.fadeOut4 = function() {
        this.isFlickeringFissure = false;
        objectManager.remove(this.fade1);
        objectManager.remove(this.fissure);

        animationManager.makeNull({endBeat: metronome.rawTime(2000), callback: this.takeStep.bind(this)});
    }

    this.fadeIntoCredits = function() {
        if (this.fakeFrame === undefined) {
            let fakeFrameCanvasThings = makeScaledCanvas(gameWidth, gameHeight);
            let fakeFrameCanvas = fakeFrameCanvasThings.canvas;
            let fakeFrameContext = fakeFrameCanvasThings.context;
            objectManager.drawFrame(fakeFrameContext);
            this.fakeFrame = new Bitmap({width: gameWidth, height: gameHeight, x: 0, y: 0, image: fakeFrameCanvas, isFree: true});
        }

        objectManager.add(this.fakeFrame);
        var now = metronome.rawTime();
        animationManager.makeTween({object: this.fade2, property: "alpha", startValue: 1, endValue: 0, startBeat: now, endBeat: now.rawAdvancedBy(1000), callback: this.takeStep.bind(this)});
    }

    this.currentStep = 0;
    this.steps = [this.crash1.bind(this), this.crash2.bind(this), this.crash3.bind(this), this.crash4.bind(this), this.suburbs1.bind(this), this.suburbs2.bind(this), this.suburbs3.bind(this), this.suburbs4.bind(this), this.suburbs5.bind(this), this.suburbs6.bind(this), this.suburbs7.bind(this), this.suburbs8.bind(this), this.explode.bind(this), this.finishExplode.bind(this), this.fadeOut1.bind(this), this.fadeOut2.bind(this), this.fadeOut3.bind(this), this.fadeOut4.bind(this), this.fadeIntoCredits.bind(this)];

    this.rFlickerMax = 0, this.gbFlickerMax = 0;
    this.isClamoring = false, this.isFlickeringFissure = false;

    this.shards = [];
    this.background = new Rectangle({centerX: gameWidth / 2, centerY: gameHeight / 2, width: gameWidth, height: gameHeight, color: "black", isFree: true, zIndex: -10});
    // Screen spillover on this for screen shaking
    this.flash = new Rectangle({centerX: gameWidth / 2, centerY: gameHeight / 2, width: gameWidth + screenShakeIntensity * 2, height: gameHeight + screenShakeIntensity * 2, color: "white", isFree: true, zIndex: 10});

    let suburbsCanvasWidth = gameWidth + 20;
    let suburbsCanvasHeight = gameHeight + 20;
    let suburbsCanvasThings = makeScaledCanvas(suburbsCanvasWidth, suburbsCanvasHeight);
    let suburbsCanvas = suburbsCanvasThings.canvas;
    let suburbsCanvasContext = suburbsCanvasThings.context;
    suburbsCanvasContext.clearRect(0, 0, suburbsCanvasWidth, suburbsCanvasHeight);
    suburbsCanvasContext.save();
    suburbsCanvasContext.translate(10, 10);
    var roadLines = [
        [{x: -10, y: 28}, {x: 75, y: 36}, {x: 91.6, y: -10}],
        [{x: 127.6, y: -10}, {x: 110, y: 39}, {x: 322, y: 58}, {x: 341.9, y: -10}],
        [{x: 375.6, y: -10}, {x: 357, y: 61}, {x: 547, y: 78}, {x: 560.5, y: -10}],
        [{x: 592.1, y: -10}, {x: 582, y: 81}, {x: 810, y: 99.8}],
        [{x: -10, y: 49.1}, {x: 68, y: 56}, {x: -10, y: 291.1}],
        [{x: 103, y: 60}, {x: 317, y: 79}, {x: 234, y: 361}, {x: 11, y: 338}, {x: 103, y: 60}],
        [{x: 351, y: 82}, {x: 544, y: 100}, {x: 501, y: 391}, {x: 279, y: 367}, {x: 351, y: 82}],
        [{x: 810, y: 122.9}, {x: 580, y: 103}, {x: 549, y: 396}, {x: 810, y: 424}],
        [{x: -10, y: 374}, {x: 224, y: 398}, {x: 163.1, y: 610}],
        [{x: 216.5, y: 610}, {x: 269, y: 403}, {x: 496, y: 427}, {x: 469.6, y: 610}],
        [{x: 527, y: 610}, {x: 545, y: 432}, {x: 810, y: 460}]
    ];
    suburbsCanvasContext.lineWidth = 4;
    suburbsCanvasContext.strokeStyle = "black";
    for (var i = 0; i < roadLines.length; i++) {
        var roadLine = roadLines[i];
        suburbsCanvasContext.beginPath();
        suburbsCanvasContext.moveTo(roadLine[0].x, roadLine[0].y);
        for (var j = 1; j < roadLine.length; j++) {
            suburbsCanvasContext.lineTo(roadLine[j].x, roadLine[j].y);
        }
        suburbsCanvasContext.stroke();
    }

    var houses = [
        // Block 1, 1
        new House({x: 0, y: -20, rotate: 11, scale: .92, width: 70, roofHeight: 15, baseHeight: 15}),

        // Block 1, 2
        new House({x: 125, y: -8, rotate: 10, scale: .92, width: 70, roofHeight: 15, baseHeight: 15}),
        new House({x: 235, y: -10, rise: 9, roofHeight: 24, width: 90, scale: .92, rotate: 9}),

        // Block 1, 3
        new House({x: 370, y: 2, rotate: 8, scale: .92}),
        new House({x: 495, y: -40, rotate: 7, scale: .90, roofHeight: 20}),
        new House({x: 470, y: 22, rotate: 7, scale: .92, width: 80, roofHeight: 17, baseHeight: 15}),

        // Block 1, 4
        new House({x: 615, y: 10, rotate: 6, scale: .92, width: 100, roofHeight: 30, baseHeight: 20, rise: 9}),
        new House({x: 770, y: 8, rotate: 5, scale: .92, width: 90, roofHeight: 50, baseHeight: 15, rise: 9}),

        // Block 2, 1
        new House({x: -20, y: 20, rotate: 11, scale: .94, width: 75, baseHeight: 16, rise: 9}),
        new House({x: -45, y: 90, rotate: 11, scale: .96, width: 80, roofHeight: 17, baseHeight: 15}),
        new House({x: -80, y: 158, rise: 9, roofHeight: 24, width: 90, scale: .98, rotate: 10}),

        // Block 2, 2
        new House({x: 105, y: 43, roofHeight: 18, rotate: 10, scale: .94}),
        new House({x: 85, y: 105, rotate: 10, scale: .96, width: 80, baseHeight: 15}),
        new House({x: 58, y: 179, rotate: 9, scale: .98, width: 75, baseHeight: 16, rise: 9}),
        new House({x: 28, y: 282, rotate: 9, width: 70, roofHeight: 15, baseHeight: 15}),
        new House({x: 223, y: 60, rotate: 9, scale: .94, width: 80, roofHeight: 17, baseHeight: 15}),
        new House({x: 213, y: 129, rotate: 9, scale: .96, roofHeight: 20}),
        new House({x: 190, y: 198, rotate: 8, scale: .98}),
        new House({x: 140, y: 284, rise: 9, roofHeight: 24, width: 90, rotate: 8}),

        // Block 2, 3
        new House({x: 355, y: 72, rotate: 8, scale: .94, width: 80, baseHeight: 15}),
        new House({x: 335, y: 155, rotate: 8, scale: .96, width: 70, roofHeight: 15, baseHeight: 15}),
        new House({x: 315, y: 224, roofHeight: 18, rotate: 7, scale: .98}),
        new House({x: 294, y: 303, rise: 9, roofHeight: 24, width: 90, rotate: 7}),
        new House({x: 475, y: 90, rotate: 7, scale: .94, roofHeight: 20}),
        new House({x: 452, y: 158, rotate: 7, scale: .96, width: 75, baseHeight: 16, rise: 9}),
        // (this.house)
        new House({x: 430, y: 318, rotate: 6}),

        // Block 2, 4
        new House({x: 605, y: 90, rotate: 6, scale: .94, width: 90, roofHeight: 50, baseHeight: 15, rise: 9}),
        new House({x: 590, y: 210, rotate: 6, scale: .96, width: 110, roofHeight: 35}),
        new House({x: 575, y: 315, rotate: 5, width: 100, roofHeight: 30, baseHeight: 20, rise: 9}),
        new House({x: 735, y: 116, rotate: 5, scale: .94, width: 125, roofHeight: 40, baseHeight: 15}),
        new House({x: 755, y: 214, rotate: 5, scale: .96, width: 90, roofHeight: 45, baseHeight: 20}),
        new House({x: 730, y: 328, rotate: 4, width: 120, roofHeight: 28, baseHeight: 24, rise: 8}),

        // Block 3, 2
        new House({x: 6, y: 355, rise: 9, roofHeight: 24, width: 90, rotate: 9, scale: 1.02}),
        new House({x: -16, y: 438, roofHeight: 18, rotate: 9, scale: 1.04}),
        new House({x: -50, y: 537, rise: 9, roofHeight: 24, width: 90, rotate: 9, scale: 1.06}),
        new House({x: 140, y: 379, rotate: 8, scale: 1.02, roofHeight: 20}),
        new House({x: 100, y: 460, rotate: 8, scale: 1.04, width: 80, roofHeight: 17, baseHeight: 15}),
        new House({x: 76, y: 565, rotate: 8, scale: 1.06, width: 70, roofHeight: 15, baseHeight: 15}),

        // Block 3, 3
        new House({x: 272, y: 389, rotate: 7, scale: 1.02}),
        new House({x: 251, y: 481, rotate: 7, scale: 1.04, width: 80, roofHeight: 17, baseHeight: 15}),
        new House({x: 228, y: 570, rotate: 7, scale: 1.06, roofHeight: 20}),
        new House({x: 415, y: 418, rotate: 6, scale: 1.02, width: 70, roofHeight: 15, baseHeight: 15}),
        new House({x: 390, y: 488, rotate: 6, scale: 1.04, width: 80, baseHeight: 15}),
        new House({x: 380, y: 570, rotate: 6, scale: 1.06, width: 75, baseHeight: 16, rise: 9}),

        // Block 3, 4
        new House({x: 560, y: 430, rotate: 5, scale: 1.02, width: 125, roofHeight: 40, baseHeight: 15}),
        new House({x: 550, y: 550, rotate: 5, scale: 1.04, width: 90, roofHeight: 45, baseHeight: 20}),
        new House({x: 730, y: 431, rotate: 4, scale: 1.02, width: 90, roofHeight: 50, baseHeight: 15, rise: 9}),
        new House({x: 710, y: 565, rotate: 4, scale: 1.04, width: 110, roofHeight: 35})
    ];
    for (var i = 0; i < houses.length; i++) {
        houses[i].draw(suburbsCanvasContext);
    }
    suburbsCanvasContext.restore();
    this.suburbs = new Bitmap({image: suburbsCanvas, width: suburbsCanvasWidth, height: suburbsCanvasHeight, x: -10, y: -10, isFree: true, zIndex: -10});
    this.house = new House({x: 432, y: 242, isFree: true, rotate: 6, scale: .98, width: 80, roofHeight: 17, baseHeight: 15, zIndex: 2});
    this.fissure = new Fissure({x: 412, y: 258, isFree: true, rotate: 8});
    this.plume = new Plume({x: 460, y: -20, height: 289, width: 65, isFree: true, rotate: 9, zIndex: 1});
    this.fade1 = new Rectangle({centerX: gameWidth / 2, centerY: gameHeight / 2, isFree: true, width: gameWidth, height: gameHeight, color: "black", zIndex: -1});
    this.fade2 = new Rectangle({centerX: gameWidth / 2, centerY: gameHeight / 2, isFree: true, width: gameWidth, height: gameHeight, color: "black", zIndex: 1});
    // We can only draw this after the object manager is instantiated
    this.fakeFrame = undefined;
}

EndingSceneController.prototype.draw = function(timestamp) {

}

EndingSceneController.prototype.startTransitionIn = function(transitionType) {
    this.currentStep = 0;
    this.rFlickerMax = 0, this.gbFlickerMax = 0;
    this.isClamoring = false, this.isFlickeringFissure = false;

    objectManager.dvdScale = 0;
    objectManager.remove(objectManager.objects);

    let clonedCanvasThings = makeScaledCanvas(gameWidth, gameHeight);
    let clonedCanvas = clonedCanvasThings.canvas;
    let clonedContext = clonedCanvasThings.context;
    clonedContext.drawImage(canvas, 0, 0, gameWidth, gameHeight);

    var points = [
        {x: 0, y: 0},
        {x: 45, y: 0},
        {x: 63, y: 189},
        {x: 0, y: 157},
        {x: 90, y: 471},
        {x: 0, y: 518},
        {x: 102, y: 600},
        {x: 0, y: 600},
        {x: 207, y: 0},
        {x: 553, y: 130},
        {x: 451, y: 298},
        {x: 360, y: 341},
        {x: 410, y: 366},
        {x: 267, y: 600},
        {x: 633, y: 0},
        {x: 800, y: 0},
        {x: 800, y: 131},
        {x: 691, y: 183},
        {x: 800, y: 225},
        {x: 800, y: 567},
        {x: 800, y: 600}
    ];
    var polygons = [
        [0, 1, 2, 3],
        [3, 2, 4, 5],
        [5, 4, 6, 7],
        [1, 8, 9, 10, 11, 2],
        [2, 11, 4],
        [4, 11, 12, 13, 6],
        [11, 10, 12],
        [8, 14, 9],
        [14, 15, 16, 17, 9],
        [17, 16, 18],
        [9, 17, 10],
        [10, 17, 18, 19, 12],
        [12, 19, 20, 13]
    ];
    this.shards = [];
    for (var i = 0; i < polygons.length; i++) {
        this.shards.push(new Shard({source: clonedCanvas, points: points, polygon: polygons[i], isFree: true}));
    }
    objectManager.add(this.shards);
    this.background.color = "black";
    objectManager.add(this.background);
}

EndingSceneController.prototype.finishTransitionIn = function(transitionType) {
    soundPlayer.play("hellloop", {loop: true});
    this.currentStep = 0;
    this.takeStep();
}

EndingSceneController.prototype.startTransitionOut = function(transitionType) {
    soundPlayer.stopAllSounds();
    objectManager.remove(this.fade2);
    objectManager.remove(this.fakeFrame);
    objectManager.dvdScale = 1;
}

EndingSceneController.prototype.finishTransitionOut = function(transitionType) {

}

EndingSceneController.prototype.keyDown = function(key, timestamp) {

}
