// chartmanager.js
// Object for managing the note chart and timing player input.

function ChartManager() {
    this.tick = function(timestamp) {
        if (this.isPaused) {
            return;
        }
        
        this.checkMisses(timestamp);
        
        if (this.chart === undefined && this.currentBeat !== undefined && this.currentBeat.rawTimeFromNow() <= 0) {
            for (var i = 0; i < this.listeners.length; i++) {
                this.listeners[i].chartCompleted();
            }
            this.stop();
        }
    }

    this.tempoChanged = function(oldTempo) {
        this.currentBeat = this.currentBeat.reinterpretWithCurrentMetronome();
    }
    
    this.paused = function(pauseTime) {
        this.isPaused = true;
    }
    
    this.resumed = function(pausedDuration) {
        this.isPaused = false;
        this.currentBeat.startTime += pausedDuration;
        this.currentBeat.cachedTimestamp = undefined;
    }
    
    this.moveToNextNote = function() {
        if (this.currentBeat === undefined || this.chart === undefined) {
            return;
        }
        
        this.currentBeat = this.currentBeat.advancedBy(this.chart[this.currentNote].duration);
        this.currentNote += 1;
        if (this.currentNote == this.chart.length) {
            if (this.loopsLeft == 1) {
                this.chart = undefined;
                return;
            } else {
                if (this.loopsLeft != "forever") {
                    this.loopsLeft -= 1;
                }
                this.currentNote = 0;
            }
        }
    }
    
    this.checkHit = function(direction, timestamp) {
        if (this.chart[this.currentNote].direction != direction) {
            for (var i = 0; i < this.listeners.length; i++) {
                this.listeners[i].mistookNote(this.chart[this.currentNote], direction, timestamp);
            }
        } else {
            for (var i = 0; i < this.listeners.length; i++) {
                this.listeners[i].hitNote(this.chart[this.currentNote], timestamp);
            }
        }
        this.moveToNextNote();
    }
    
    this.checkMisses = function(timestamp) {
        while (this.chart && (this.currentBeat.timestamp() - timestamp) < -inputError) {
            if (pendingDirection) {
                this.checkHit(pendingDirection, pendingDirectionTimestamp);
                clearPendingDirection();
                continue;
            }
            
            for (var i = 0; i < this.listeners.length; i++) {
                this.listeners[i].missedNote(this.chart[this.currentNote]);
            }
            this.moveToNextNote();
        }
    }
    
    this.chart = undefined;
    this.currentBeat = undefined;
    this.isPaused = false;
    
    this.listeners = [];
}

ChartManager.prototype.registerListener = function(listener) {
    if (!this.listeners.includes(listener)) {
        this.listeners.push(listener);
    }
}

ChartManager.prototype.unregisterListener = function(listener) {
    var i = this.listeners.indexOf(listener);
    if (i != -1) {
        this.listeners.splice(i, 1);
    }
}

ChartManager.prototype.signal = function(direction, timestamp) {
    if (this.isPaused || this.chart === undefined) {
        return;
    }
    
    this.checkMisses(timestamp);
    if (this.chart === undefined && this.currentBeat === undefined) {
        // If this condition is true, then one of the listeners stopped the
        // chart manager in a .missedNote callback. We should just stop
        // handling the signal at that point.
        return;
    }
    
    if (this.chart === undefined || Math.abs(timestamp - this.currentBeat.timestamp()) > inputError) {
        for (var i = 0; i < this.listeners.length; i++) {
            this.listeners[i].whiffed(direction, timestamp);
        }
        return;
    }
    
    this.checkHit(direction, timestamp);
}

ChartManager.prototype.loadChart = function(chart, loops) {
    this.chart = chart.slice(0);
    this.currentNote = 0;
    this.loopsLeft = loops;
    this.currentBeat = undefined;
    metronome.unregisterListener(this);
}

ChartManager.prototype.start = function(startBeat) {
    this.currentBeat = startBeat;
    metronome.registerListener(this);
}

ChartManager.prototype.stop = function() {
    metronome.unregisterListener(this);
    this.currentBeat = undefined;
    this.chart = undefined;
}


//
// UTILITY DURATION CALCULATION
//

function chartDuration(chart) {
    var stepSizes = [];
    stepSizes[Note.thirtysecond] = 1;
    stepSizes[Note.sixteenth] = 2;
    stepSizes[Note.eighth] = 4;
    stepSizes[Note.quarter] = 8;
    stepSizes[Note.half] = 16;
    stepSizes[Note.whole] = 32;
    
    var thirtyseconds = 0;
    for (var i = 0; i < chart.length; i++) {
        if (Array.isArray(chart[i].duration)) {
            for (j = 0; j < chart[i].duration.length; j++) {
                thirtyseconds += stepSizes[chart[i].duration[j]];
            }
        } else {
            thirtyseconds += stepSizes[chart[i].duration];
        }
    }
    return thirtyseconds;
}
