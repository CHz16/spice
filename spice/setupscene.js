// setupscene.js
// Controller for the setup scene.

var setupX = 100, setupFirstY = 110;
var setupLineHeight = 40;
var setupHeaderFont = "30px sans-serif", setupOptionFont = "25px sans-serif";
var setupMaxMistakes = 5;

function SetupSceneController() {    
    this.moveSelectionArrow = function() {
        this.selectionArrow.centerX = setupX;
        if (this.selection <= setupMaxMistakes) {
            this.selectionArrow.centerY = setupFirstY + (this.selection + 1) * setupLineHeight;
        } else {
            this.selectionArrow.centerY = setupFirstY + (setupMaxMistakes + 3) * setupLineHeight;
        }
        this.selectionArrow.centerX -= 40;
        this.selectionArrow.centerY -= 10;
    }
    
    this.selection = undefined;
    this.selectionArrow = new Arrow({width: 30, height: 15, color: "blue", rightHead: 5, zIndex: 10});
    
    this.labels = [];
    this.labels.push(new Label({text: "Allowed Mistakes per Level:", font: setupHeaderFont, x: setupX, y: setupFirstY}));
    for (var i = 0; i <= setupMaxMistakes; i++) {
        this.labels.push(new Label({text: i, font: setupOptionFont, x: setupX, y: setupFirstY + (i + 1) * setupLineHeight}));
    }
    this.labels.push(new Label({x: setupX, y: setupFirstY + (setupMaxMistakes + 3) * setupLineHeight, text: "Main Menu", font: setupHeaderFont}));
    
    this.photo = new Bitmap({x: 300, y: 200, image: imagePreloader.getImage("stockphoto1"), rotate: 15, scale: .75, zIndex: -1});
}

SetupSceneController.prototype.draw = function(timestamp) {
    
}

SetupSceneController.prototype.startTransitionIn = function(transitionType) {
    this.selection = allowedMistakes;
    this.moveSelectionArrow();
}

SetupSceneController.prototype.finishTransitionIn = function(transitionType) {
    objectManager.add(this.selectionArrow);
    objectManager.add(this.labels);
    objectManager.add(this.photo);
}

SetupSceneController.prototype.startTransitionOut = function(transitionType) {
    objectManager.remove(this.selectionArrow);
}

SetupSceneController.prototype.finishTransitionOut = function(transitionType) {
    objectManager.remove(this.labels);
    objectManager.remove(this.photo);
}

SetupSceneController.prototype.keyDown = function(key, timestamp) {
    if (key == Key.up) {
        this.selection = (this.selection + setupMaxMistakes + 1) % (setupMaxMistakes + 2);
        this.moveSelectionArrow();
    } else if (key == Key.down) {
        this.selection = (this.selection + 1) % (setupMaxMistakes + 2);
        this.moveSelectionArrow();
    } else if (key == Key.action && this.selection <= setupMaxMistakes) {
        allowedMistakes = this.selection;
        startTransition(0, Transition.seek);
    } else if (key == Key.action || key == Key.cancel) {
        startTransition(0, Transition.seek);
    }
}
