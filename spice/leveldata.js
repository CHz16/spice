// leveldata.js
// All the data needed to run us some levels

// Included here because of import order
var Key = {up: 0, down: 1, left: 2, right: 3, upleft: 4, upright: 5, downleft: 6, downright: 7, action: 8, cancel: 9};

var levels = [
    { // Level 1
        name: "Basics",
        tempo: 100,
        intro: [
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter}
               ],
        pattern: [
                     {channels: [3], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth}
                 ],
        objects: [
                     {name: "prompt", type: Arrow, args: {centerX: 150, centerY: 300, width: 200, height: 50, rightHead: 10, color: "blue"}}
                 ],
        chart: [
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.left, duration: Note.quarter}
               ],
        loops: 4,
        introStepLength: 5,
        steps: [
                   {
                       duration: Note.whole,
                       animations: []
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   // body
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   }
               ],
        failRoutines: [
                          [ // 0
                              {object: "prompt", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 30, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ],
                          [ // 1
                              {object: "prompt", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: -30, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth}
                          ]
                      ],
        reset: [
                   {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                   {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth},
                   {object: "prompt", property: "width", endValue: 200, duration: Note.sixteenth},
                   {object: "prompt", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                   {object: "prompt", property: "rotate", endValue: 0, duration: Note.sixteenth}
               ]
    },
    { // Level 2
        name: "Mix-Up",
        tempo: 120,
        intro: [
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [3], duration: Note.eighth}
               ],
        pattern: [
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [5], duration: Note.eighth},
                     {channels: [9], duration: Note.eighth},
                     {channels: [6], duration: Note.eighth},
                     {channels: [7], duration: Note.eighth},
                     {channels: [3, 8], duration: Note.eighth}
                 ],
        objects: [
                     {name: "prompt", type: Arrow, args: {centerX: 150, centerY: 300, width: 200, height: 50, rightHead: 10, color: "blue"}}
                 ],
        chart: [
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.left, duration: Note.quarter},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.left, duration: Note.eighth},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.left, duration: Note.eighth}
               ],
        chartDelay: [Note.whole, Note.whole],
        loops: 3,
        introStepLength: 5,
        steps: [
                   {
                       duration: Note.whole,
                       animations: []
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   // body
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   }
               ],
        failRoutines: [
                          [ // 0
                              {object: "prompt", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 30, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ],
                          [ // 1
                              {object: "prompt", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: -30, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ]
                      ],
        reset: [
                   {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                   {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth},
                   {object: "prompt", property: "width", endValue: 200, duration: Note.sixteenth},
                   {object: "prompt", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                   {object: "prompt", property: "rotate", endValue: 0, duration: Note.sixteenth}
               ]
    },
    { // Level 3
        name: "Rod & Reel",
        tempo: 110,
        intro: [
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter}
               ],
        pattern: [
                     {channels: [3, 8], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [11], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [11], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [11], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth}
                 ],
        objects: [
                     {name: "prompt", type: Arrow, args: {centerX: 650, centerY: 300, width: 200, height: 50, leftHead: 10, color: "blue"}}
                 ],
        chart: [
                   {direction: Key.left, duration: Note.quarter},
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.right, duration: Note.quarter}
               ],
        loops: 3,
        introStepLength: 5,
        hellPeeks: 1,
        hellLevel: 0.1,
        steps: [
                   {
                       duration: Note.whole,
                       animations: []
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   // body
                   {
                       duration: Note.quarter,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", endValue: 660, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerX", endValue: 380, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -15, duration: Note.sixteenth},
                                       {object: "prompt", property: "rotate", type: "pulse", pulseDisplacement: 5, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endIncrement: 23, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: -46, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endIncrement: 23, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: -46, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endIncrement: 23, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: -46, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endIncrement: 23, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: -46, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endIncrement: 23, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: -46, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endIncrement: 23, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: -46, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endIncrement: 23, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: -46, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endIncrement: 23, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: -46, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endIncrement: 23, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: -46, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endValue: 200, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   }
               ],
        failRoutines: [
                          [ // 0
                              {object: "prompt", property: "centerX", endValue: -100, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: -75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endValue: 90, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ],
                          [ // 1
                              {object: "prompt", property: "centerX", endIncrement: 350, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 30, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ]
                      ],
        reset: [
                   {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                   {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth},
                   {object: "prompt", property: "width", endValue: 200, duration: Note.sixteenth},
                   {object: "prompt", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                   {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth},
                   {object: "prompt", property: "rotate", endValue: 0, duration: Note.sixteenth}
               ]
    },
    { // Level 4
        name: "Spinning Swing",
        tempo: 85,
        intro: [
                   {channels: [2], duration: [Note.quarter, Note.eighth]},
                   {channels: [2], duration: [Note.quarter, Note.eighth]},
                   {channels: [2, 12], duration: Note.eighth},
                   {channels: [2], duration: Note.eighth},
                   {channels: [2], duration: Note.eighth},
                   {channels: [2, 12], duration: Note.eighth},
                   {channels: [2, 12], duration: Note.eighth},
                   {channels: [2, 12], duration: Note.eighth},
                   {channels: [1], duration: Note.eighth}
               ],
        pattern: [
                     {channels: [3], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [3], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [3], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [3], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1, 8], duration: Note.eighth}
                 ],
        objects: [
                     {name: "prompt", type: Arrow, args: {centerX: 400, centerY: 300, width: 200, rotate: 45, height: 50, rightHead: 10, color: "blue"}}
                 ],
        chart: [
                   {direction: Key.downright, duration: Note.eighth},
                   {direction: Key.upleft, duration: Note.quarter},
                   {direction: Key.downleft, duration: Note.eighth},
                   {direction: Key.upright, duration: Note.quarter},
                   {direction: Key.upleft, duration: Note.eighth},
                   {direction: Key.downright, duration: Note.quarter},
                   {direction: Key.upright, duration: Note.eighth},
                   {direction: Key.downleft, duration: Note.quarter}
               ],
        chartDelay: [Note.whole, Note.half],
        barLength: 3,
        loops: 2,
        introStepLength: 5,
        steps: [
                   {
                       duration: [Note.quarter, Note.quarter, Note.quarter],
                       animations: []
                   },
                   {
                       duration: [Note.quarter, Note.eighth],
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   // body
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 600, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 500, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 400, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "rotate", endValue: 135, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 2,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 200, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 500, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 3,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 400, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 3,
                       animations: [
                                       {object: "prompt", property: "rotate", endValue: 225, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 4,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 200, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 100, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 5,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 400, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 5,
                       animations: [
                                       {object: "prompt", property: "rotate", endValue: 315, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 6,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 600, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 100, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 7,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 400, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 7,
                       finish: function() { this.namedObjects["prompt"].rotate = 45; },
                       animations: [
                                       {object: "prompt", property: "rotate", endValue: 405, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   }
               ],
        failRoutines: [
                          [ // 0
                              {object: "prompt", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 30, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ],
                          [ // 1
                              {object: "prompt", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: -75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 60, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ],
                          [ // 2
                              {object: "prompt", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 30, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ],
                          [ // 3
                              {object: "prompt", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: -75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 60, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ],
                          [ // 4
                              {object: "prompt", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: -75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 30, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ],
                          [ // 5
                              {object: "prompt", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 60, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ],
                          [ // 6
                              {object: "prompt", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: -75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 30, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ],
                          [ // 7
                              {object: "prompt", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 60, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ]
                      ],
        reset: [
                   {object: "prompt", property: "centerX", endValue: 400, duration: Note.sixteenth},
                   {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth},
                   {object: "prompt", property: "width", endValue: 200, duration: Note.sixteenth},
                   {object: "prompt", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                   {object: "prompt", property: "rotate", endValue: 45, duration: Note.sixteenth}
               ]
    },
    { // Level 5
        name: "Poly",
        tempo: 110,
        intro: [
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [3], duration: Note.quarter}
               ],
        pattern: [
                     {channels: [4], duration: Note.quarter},
                     {channels: [1], duration: Note.quarter},
                     {channels: [4], duration: Note.quarter},
                     {channels: [1], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [3, 1, 11], duration: Note.quarter},
                     {channels: [4], duration: Note.quarter},
                     {channels: [3], duration: Note.quarter},
                     {channels: [1], duration: Note.quarter},
                     {channels: [3], duration: Note.quarter},
                     {channels: [1], duration: Note.quarter},
                     {channels: [3, 1, 11], duration: Note.eighth},
                     {channels: [3, 1, 11], duration: Note.eighth},
                     {channels: [3, 1, 11], duration: Note.eighth},
                     {channels: [3, 1, 11], duration: Note.eighth},
                     {channels: [3, 1, 11], duration: Note.quarter},
                     {channels: [0], duration: Note.quarter},
                     {channels: [3, 8], duration: Note.quarter}
                 ],
        objects: [
                     {name: "prompt", type: Arrow, args: {centerX: 330, centerY: 370, width: 200, height: 50, rightHead: 10, rotate: -45, color: "blue"}},
                     {name: "uphead", type: Arrow, args: {centerX: 400, centerY: 300, width: 0, height: 50, rotate: -90, color: "blue"}},
                     {name: "righthead", type: Arrow, args: {centerX: 400, centerY: 300, width: 0, height: 50, color: "blue"}}
                 ],
        chart: [
                   {direction: Key.upright, duration: Note.half},
                   {direction: Key.upright, duration: Note.half},
                   {direction: Key.upright, duration: Note.eighth},
                   {direction: Key.upright, duration: Note.eighth},
                   {direction: Key.upright, duration: Note.eighth},
                   {direction: Key.upright, duration: Note.eighth},
                   {direction: Key.upright, duration: Note.half},
                   {direction: Key.up, duration: Note.quarter},
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.up, duration: Note.quarter},
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.upright, duration: Note.eighth},
                   {direction: Key.upright, duration: Note.eighth},
                   {direction: Key.upright, duration: Note.eighth},
                   {direction: Key.upright, duration: Note.eighth},
                   {direction: Key.upright, duration: Note.quarter},
                   {direction: Key.downleft, duration: Note.quarter}
               ],
        chartDelay: [Note.whole, Note.whole],
        loops: 2,
        introStepLength: 5,
        hellPeeks: 2,
        hellLevel: 0.3,
        steps: [
                   {
                       duration: Note.whole,
                       animations: []
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   // body
                   {
                       duration: Note.quarter,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 40, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -40, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: 6, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", endIncrement: 6, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 40, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -40, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: 6, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", endIncrement: 6, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 40, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -40, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: 6, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 40, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -40, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: 6, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 40, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -40, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: 6, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 40, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -40, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: 6, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "width", endIncrement: 6, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.thirtysecond},
                                       {object: "uphead", property: "centerY", endValue: 200, duration: Note.sixteenth},
                                       {object: "uphead", property: "width", endValue: 200, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerX", endValue: 500, duration: Note.sixteenth},
                                       {object: "righthead", property: "width", endValue: 200, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "uphead", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "uphead", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "uphead", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "righthead", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "righthead", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "uphead", property: "rightHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "righthead", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "uphead", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "righthead", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "uphead", property: "rightHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "righthead", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "uphead", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 2, // temp
                       animations: [
                                       {object: "prompt", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "uphead", property: "rightHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerX", type: "pulse", pulseDisplacement: 75, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerY", type: "pulse", pulseDisplacement: -75, duration: Note.sixteenth},
                                       {object: "righthead", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.sixteenth,
                       failRoutine: 2, // temp
                       animations: [
                                       {object: "prompt", property: "centerX", endIncrement: -40, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endIncrement: 40, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: -27, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 5, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 5, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerY", endValue: 340, duration: Note.sixteenth},
                                       {object: "uphead", property: "width", endValue: 0, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerX", endValue: 360, duration: Note.sixteenth},
                                       {object: "righthead", property: "width", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: [Note.eighth, Note.sixteenth],
                       failRoutine: 2, // temp
                       animations: [
                                       {object: "prompt", property: "centerX", endIncrement: 40, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endIncrement: -40, duration: Note.sixteenth},
                                       {object: "prompt", property: "width", endIncrement: -27, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "uphead", property: "centerY", endIncrement: -40, duration: Note.sixteenth},
                                       {object: "righthead", property: "centerX", endIncrement: 40, duration: Note.sixteenth}
                                   ]
                   }
               ],
        failRoutines: [
                          [ // 0
                              {object: "prompt", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endValue: 550, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ],
                          [ // 1
                              {object: "prompt", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endValue: 550, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                              {object: "uphead", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "uphead", property: "centerY", endValue: 545, duration: Note.sixteenth},
                              {object: "uphead", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "uphead", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                              {object: "righthead", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "righthead", property: "centerY", endValue: 555, duration: Note.sixteenth},
                              {object: "righthead", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "righthead", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ],
                          [ // 2
                              {object: "prompt", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endValue: 550, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                              {object: "uphead", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "uphead", property: "centerY", endValue: 545, duration: Note.sixteenth},
                              {object: "uphead", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "uphead", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                              {object: "righthead", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "righthead", property: "centerY", endValue: 555, duration: Note.sixteenth},
                              {object: "righthead", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "righthead", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ]
                      ],
        reset: [
                   {object: "prompt", property: "centerX", endValue: 330, duration: Note.sixteenth},
                   {object: "prompt", property: "centerY", endValue: 370, duration: Note.sixteenth},
                   {object: "prompt", property: "width", endValue: 200, duration: Note.sixteenth},
                   {object: "prompt", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                   {object: "prompt", property: "rotate", endValue: -45, duration: Note.sixteenth},
                   {object: "uphead", property: "centerX", endValue: 400, duration: Note.sixteenth},
                   {object: "uphead", property: "centerY", endValue: 300, duration: Note.sixteenth},
                   {object: "uphead", property: "width", endValue: 0, duration: Note.sixteenth},
                   {object: "uphead", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "uphead", property: "rightHead", endValue: 0, duration: Note.sixteenth},
                   {object: "uphead", property: "rotate", endValue: -90, duration: Note.sixteenth},
                   {object: "righthead", property: "centerX", endValue: 400, duration: Note.sixteenth},
                   {object: "righthead", property: "centerY", endValue: 300, duration: Note.sixteenth},
                   {object: "righthead", property: "width", endValue: 0, duration: Note.sixteenth},
                   {object: "righthead", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "righthead", property: "rightHead", endValue: 0, duration: Note.sixteenth}
               ]
    },
    { // Level 6
        name: "Blindfold Play",
        tempo: 140,
        intro: [
                   {channels: [2], duration: [Note.half, Note.quarter]},
                   {channels: [2], duration: [Note.half, Note.quarter]},
                   {channels: [2], duration: [Note.quarter, Note.eighth]},
                   {channels: [2], duration: [Note.quarter, Note.eighth]},
                   {channels: [2], duration: [Note.quarter, Note.eighth]},
                   {channels: [2], duration: [Note.quarter, Note.eighth]},
                   {channels: [3], duration: Note.quarter}
               ],
        pattern: [
                     {channels: [3], duration: Note.eighth},
                     {channels: [1], duration: Note.quarter},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3], duration: Note.quarter},
                     {channels: [3], duration: Note.eighth},
                     {channels: [1], duration: Note.quarter},
                     {channels: [4], duration: Note.eighth},
                     {channels: [7], duration: Note.quarter},
                     {channels: [7], duration: Note.eighth},
                     {channels: [9], duration: Note.quarter},
                     {channels: [4], duration: Note.eighth},
                     {channels: [9], duration: Note.sixteenth},
                     {channels: [7], duration: Note.sixteenth},
                     {channels: [9], duration: Note.eighth},
                     {channels: [7], duration: Note.eighth},
                     {channels: [9], duration: Note.quarter},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3, 8], duration: Note.quarter}
                 ],
        objects: [
                     {name: "prompt", type: Arrow, args: {centerX: 150, centerY: 300, width: 200, height: 50, rightHead: 10, color: "blue"}}
                 ],
        chart: [
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.left, duration: Note.eighth},
                   {direction: Key.right, duration: [Note.quarter, Note.eighth]},
                   {direction: Key.left, duration: Note.quarter},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.left, duration: [Note.quarter, Note.eighth]},
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.left, duration: Note.eighth},
                   {direction: Key.right, duration: [Note.quarter, Note.eighth]},
                   {direction: Key.left, duration: Note.sixteenth},
                   {direction: Key.right, duration: Note.sixteenth},
                   {direction: Key.left, duration: Note.eighth},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.left, duration: [Note.quarter, Note.eighth]}
               ],
        chartDelay: [Note.whole, Note.whole, Note.whole],
        barLength: 6,
        loops: 3,
        introStepLength: 5,
        steps: [
                   {
                       duration: [Note.whole, Note.half],
                       animations: []
                   },
                   {
                       duration: [Note.quarter, Note.eighth],
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: [Note.quarter, Note.eighth],
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: [Note.quarter, Note.eighth],
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: [Note.quarter, Note.eighth],
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth}
                                   ]
                   },
                   // body
                   {
                       duration: Note.quarter,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "alpha", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.sixteenth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.sixteenth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.sixteenth},
                                       {object: "prompt", property: "alpha", endValue: 1.0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                           ]
                   }
               ],
        failRoutines: [
                          [ // 0
                              {object: "prompt", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 30, duration: Note.sixteenth},
                              {object: "prompt", property: "alpha", endValue: 1.0, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ],
                          [ // 1
                              {object: "prompt", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: -30, duration: Note.sixteenth},
                              {object: "prompt", property: "alpha", endValue: 1.0, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ]
                      ],
        reset: [
                   {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                   {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth},
                   {object: "prompt", property: "width", endValue: 200, duration: Note.sixteenth},
                   {object: "prompt", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                   {object: "prompt", property: "rotate", endValue: 0, duration: Note.sixteenth},
                   {object: "prompt", property: "alpha", endValue: 1, duration: Note.sixteenth}
               ]
    },
    { // Level 7
        name: "Riding",
        tempo: 110,
        intro: [
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [3], duration: Note.eighth}
               ],
        pattern: [
                     {channels: [4], duration: Note.quarter},
                     {channels: [1], duration: Note.quarter},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3], duration: Note.quarter},
                     {channels: [1], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3, 8], duration: Note.eighth}
                 ],
        objects: [
                     {name: "prompt", type: Arrow, args: {centerX: 400, centerY: 140, width: 200, height: 50, rotate: 90, rightHead: 10, color: "blue"}}
                 ],
        chart: [
                   {direction: Key.down, duration: Note.eighth},
                   {direction: Key.up, duration: Note.quarter},
                   {direction: Key.down, duration: Note.quarter},
                   {direction: Key.up, duration: Note.quarter},
                   {direction: Key.down, duration: Note.eighth},
                   {direction: Key.up, duration: Note.eighth},
                   {direction: Key.up, duration: Note.quarter},
                   {direction: Key.down, duration: Note.quarter},
                   {direction: Key.up, duration: Note.eighth},
                   {direction: Key.up, duration: Note.quarter}
               ],
        chartDelay: [Note.whole, Note.whole],
        loops: 3,
        introStepLength: 5,
        steps: [
                   {
                       duration: Note.whole,
                       animations: []
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   // body
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 460, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 140, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 460, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 140, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 460, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 140, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 460, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 140, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   }
               ],
        failRoutines: [
                          [ // 0
                              {object: "prompt", property: "centerX", endValue: 300, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endValue: 550, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ],
                          [ // 1
                              {object: "prompt", property: "centerX", endValue: 900, duration: Note.eighth},
                              {object: "prompt", property: "centerY", endValue: 100, duration: Note.eighth},
                              {object: "prompt", property: "rotate", endIncrement: 180, duration: Note.eighth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.eighth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.eighth}
                          ]
                      ],
        reset: [
                   {object: "prompt", property: "centerX", endValue: 400, duration: Note.sixteenth},
                   {object: "prompt", property: "centerY", endValue: 140, duration: Note.sixteenth},
                   {object: "prompt", property: "width", endValue: 200, duration: Note.sixteenth},
                   {object: "prompt", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                   {object: "prompt", property: "rotate", endValue: 90, duration: Note.sixteenth}
               ]
    },
    { // Level 8
        name: "Anticipation",
        tempo: 110,
        intro: [
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [3], duration: Note.eighth},
               ],
        pattern: [
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3, 5], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1, 5], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3, 5], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1, 7], duration: Note.eighth},
                     {channels: [5], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [5], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [5], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [5], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [5], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [5, 7], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [5, 7], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [5, 7], duration: Note.eighth},
                     {channels: [5, 7], duration: Note.eighth},
                     {channels: [5, 7], duration: Note.eighth},
                     {channels: [3, 8], duration: Note.eighth}
                 ],
        objects: [
                     {name: "1", type: Arrow, args: {centerX: 700, centerY: 300, width: 100, height: 50, leftHead: 10, color: "blue"}},
                     {name: "2", type: Arrow, args: {centerX: 605, centerY: 325, width: 0, height: 50, rotate: 90, color: "blue"}},
                     {name: "3", type: Arrow, args: {centerX: 630, centerY: 150, width: 0, height: 50, color: "blue"}},
                     {name: "4", type: Arrow, args: {centerX: 485, centerY: 125, width: 0, height: 50, rotate: -90, color: "blue"}},
                     {name: "5", type: Arrow, args: {centerX: 510, centerY: 500, width: 0, height: 50, color: "blue"}},
                     {name: "6", type: Arrow, args: {centerX: 365, centerY: 525, width: 0, height: 50, rotate: 90, color: "blue"}},
                     {name: "7", type: Arrow, args: {centerX: 390, centerY: 80, width: 0, height: 50, color: "blue"}},
                     {name: "8", type: Arrow, args: {centerX: 245, centerY: 55, width: 0, height: 50, rotate: -90, color: "blue"}},
                     {name: "9", type: Arrow, args: {centerX: 270, centerY: 300, width: 0, height: 50, color: "blue"}},
                     {name: "backhead", type: Arrow, args: {centerX: 50, centerY: 300, width: 0, height: 50, color: "blue"}}
                 ],
        chart: [
                   {direction: Key.left, duration: Note.quarter},
                   {direction: Key.up, duration: Note.quarter},
                   {direction: Key.left, duration: Note.quarter},
                   {direction: Key.down, duration: Note.quarter},
                   {direction: Key.left, duration: Note.quarter},
                   {direction: Key.up, duration: Note.quarter},
                   {direction: Key.left, duration: Note.quarter},
                   {direction: Key.down, duration: Note.eighth},
                   {direction: Key.left, duration: Note.quarter},
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.up, duration: Note.quarter},
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.down, duration: Note.quarter},
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.up, duration: Note.quarter},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.down, duration: Note.eighth},
                   {direction: Key.right, duration: Note.eighth}
               ],
        chartDelay: [Note.whole, Note.whole],
        loops: 2,
        introStepLength: 5,
        hellPeeks: 3,
        hellLevel: 0.5,
        steps: [
                   {
                       duration: Note.whole,
                       animations: []
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond}
                                   ]
                   },
                   // body
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 0,
                       animations: [
                                       {object: "1", property: "width", endValue: 170, duration: Note.thirtysecond},
                                       {object: "1", property: "centerX", endValue: 665, duration: Note.thirtysecond},
                                       {object: "1", property: "leftHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 0,
                       animations: [
                                       {object: "2", property: "width", endValue: 100, duration: Note.thirtysecond},
                                       {object: "2", property: "centerY", endValue: 275, duration: Note.thirtysecond},
                                       {object: "2", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 0,
                       animations: [
                                       {object: "2", property: "width", endValue: 200, duration: Note.thirtysecond},
                                       {object: "2", property: "centerY", endValue: 225, duration: Note.thirtysecond},
                                       {object: "2", property: "leftHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 0,
                       animations: [
                                       {object: "3", property: "width", endValue: 100, duration: Note.thirtysecond},
                                       {object: "3", property: "centerX", endValue: 580, duration: Note.thirtysecond},
                                       {object: "3", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 0,
                       animations: [
                                       {object: "3", property: "width", endValue: 170, duration: Note.thirtysecond},
                                       {object: "3", property: "centerX", endValue: 545, duration: Note.thirtysecond},
                                       {object: "3", property: "leftHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 0,
                       animations: [
                                       {object: "4", property: "width", endValue: 100, duration: Note.thirtysecond},
                                       {object: "4", property: "centerY", endValue: 175, duration: Note.thirtysecond},
                                       {object: "4", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 0,
                       animations: [
                                       {object: "4", property: "width", endValue: 400, duration: Note.thirtysecond},
                                       {object: "4", property: "centerY", endValue: 325, duration: Note.thirtysecond},
                                       {object: "4", property: "leftHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 0,
                       animations: [
                                       {object: "5", property: "width", endValue: 100, duration: Note.thirtysecond},
                                       {object: "5", property: "centerX", endValue: 460, duration: Note.thirtysecond},
                                       {object: "5", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 0,
                       animations: [
                                       {object: "5", property: "width", endValue: 170, duration: Note.thirtysecond},
                                       {object: "5", property: "centerX", endValue: 425, duration: Note.thirtysecond},
                                       {object: "5", property: "leftHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 0,
                       animations: [
                                       {object: "6", property: "width", endValue: 100, duration: Note.thirtysecond},
                                       {object: "6", property: "centerY", endValue: 475, duration: Note.thirtysecond},
                                       {object: "6", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 0,
                       animations: [
                                       {object: "6", property: "width", endValue: 470, duration: Note.thirtysecond},
                                       {object: "6", property: "centerY", endValue: 290, duration: Note.thirtysecond},
                                       {object: "6", property: "leftHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 0,
                       animations: [
                                       {object: "7", property: "width", endValue: 100, duration: Note.thirtysecond},
                                       {object: "7", property: "centerX", endValue: 340, duration: Note.thirtysecond},
                                       {object: "7", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "7", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "7", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 0,
                       animations: [
                                       {object: "7", property: "width", endValue: 170, duration: Note.thirtysecond},
                                       {object: "7", property: "centerX", endValue: 305, duration: Note.thirtysecond},
                                       {object: "7", property: "leftHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 0,
                       animations: [
                                       {object: "8", property: "width", endValue: 100, duration: Note.thirtysecond},
                                       {object: "8", property: "centerY", endValue: 105, duration: Note.thirtysecond},
                                       {object: "8", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "7", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "7", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "8", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "8", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 0,
                       animations: [
                                       {object: "8", property: "width", endValue: 270, duration: Note.thirtysecond},
                                       {object: "8", property: "centerY", endValue: 190, duration: Note.thirtysecond},
                                       {object: "8", property: "leftHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 0,
                       animations: [
                                       {object: "9", property: "width", endValue: 100, duration: Note.thirtysecond},
                                       {object: "9", property: "centerX", endValue: 220, duration: Note.thirtysecond},
                                       {object: "9", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "1", property: "width", endValue: 130, duration: Note.thirtysecond},
                                       {object: "1", property: "centerX", endValue: 645, duration: Note.thirtysecond},
                                       {object: "1", property: "rightHead", endValue: 10, duration: Note.thirtysecond},
                                       {object: "9", property: "width", endValue: 220, duration: Note.thirtysecond},
                                       {object: "9", property: "centerX", endValue: 160, duration: Note.thirtysecond},
                                       {object: "9", property: "leftHead", endValue: 0, duration: Note.thirtysecond},
                                       {object: "backhead", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "7", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "7", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "8", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "8", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "9", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "9", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", type: "pulse", pulseDisplacement: -6, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 1,
                       animations: [
                                       {object: "9", property: "width", endValue: 0, duration: Note.thirtysecond},
                                       {object: "9", property: "centerX", endValue: 270, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 220, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 1,
                       animations: [
                                       {object: "8", property: "width", endValue: 220, duration: Note.thirtysecond},
                                       {object: "8", property: "centerY", endValue: 175, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 245, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 285, duration: Note.thirtysecond},
                                       {object: "backhead", property: "rotate", endValue: -90, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "7", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "7", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "8", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "8", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", type: "pulse", pulseDisplacement: 6, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 1,
                       animations: [
                                       {object: "8", property: "width", endValue: 0, duration: Note.thirtysecond},
                                       {object: "8", property: "centerY", endValue: 55, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 105, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 1,
                       animations: [
                                       {object: "7", property: "width", endValue: 120, duration: Note.thirtysecond},
                                       {object: "7", property: "centerX", endValue: 330, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 270, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 80, duration: Note.thirtysecond},
                                       {object: "backhead", property: "rotate", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "7", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "7", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", type: "pulse", pulseDisplacement: -6, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 1,
                       animations: [
                                       {object: "7", property: "width", endValue: 0, duration: Note.thirtysecond},
                                       {object: "7", property: "centerX", endValue: 390, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 345, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 1,
                       animations: [
                                       {object: "6", property: "width", endValue: 420, duration: Note.thirtysecond},
                                       {object: "6", property: "centerY", endValue: 315, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 365, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 105, duration: Note.thirtysecond},
                                       {object: "backhead", property: "rotate", endValue: 90, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "6", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", type: "pulse", pulseDisplacement: -6, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 1,
                       animations: [
                                       {object: "6", property: "width", endValue: 0, duration: Note.thirtysecond},
                                       {object: "6", property: "centerY", endValue: 525, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 475, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 1,
                       animations: [
                                       {object: "5", property: "width", endValue: 120, duration: Note.thirtysecond},
                                       {object: "5", property: "centerX", endValue: 450, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 390, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 500, duration: Note.thirtysecond},
                                       {object: "backhead", property: "rotate", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "5", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", type: "pulse", pulseDisplacement: -6, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 1,
                       animations: [
                                       {object: "5", property: "width", endValue: 0, duration: Note.thirtysecond},
                                       {object: "5", property: "centerX", endValue: 510, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 460, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 1,
                       animations: [
                                       {object: "4", property: "width", endValue: 350, duration: Note.thirtysecond},
                                       {object: "4", property: "centerY", endValue: 300, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 485, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 475, duration: Note.thirtysecond},
                                       {object: "backhead", property: "rotate", endValue: -90, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "4", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", type: "pulse", pulseDisplacement: 6, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 1,
                       animations: [
                                       {object: "4", property: "width", endValue: 0, duration: Note.thirtysecond},
                                       {object: "4", property: "centerY", endValue: 125, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 175, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 1,
                       animations: [
                                       {object: "3", property: "width", endValue: 120, duration: Note.thirtysecond},
                                       {object: "3", property: "centerX", endValue: 570, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 510, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 150, duration: Note.thirtysecond},
                                       {object: "backhead", property: "rotate", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "1", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "1", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "2", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "width", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "3", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "height", type: "pulse", pulseDisplacement: 12, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", type: "pulse", pulseDisplacement: -6, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 1,
                       animations: [
                                       {object: "3", property: "width", endValue: 0, duration: Note.thirtysecond},
                                       {object: "3", property: "centerX", endValue: 630, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 580, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 1,
                       animations: [
                                       {object: "2", property: "width", endValue: 150, duration: Note.thirtysecond},
                                       {object: "2", property: "centerY", endValue: 250, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 605, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 175, duration: Note.thirtysecond},
                                       {object: "backhead", property: "rotate", endValue: 90, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 1,
                       animations: [
                                       {object: "2", property: "width", endValue: 0, duration: Note.thirtysecond},
                                       {object: "2", property: "centerY", endValue: 325, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 275, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 1,
                       animations: [
                                       {object: "1", property: "width", endValue: 80, duration: Note.thirtysecond},
                                       {object: "1", property: "centerX", endValue: 670, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerX", endValue: 630, duration: Note.thirtysecond},
                                       {object: "backhead", property: "centerY", endValue: 300, duration: Note.thirtysecond},
                                       {object: "backhead", property: "rotate", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.thirtysecond,
                       failRoutine: 1,
                       animations: [
                                       {object: "1", property: "width", endValue: 100, duration: Note.thirtysecond},
                                       {object: "1", property: "centerX", endValue: 700, duration: Note.thirtysecond},
                                       {object: "1", property: "leftHead", endValue: 10, duration: Note.thirtysecond},
                                       {object: "1", property: "rightHead", endValue: 0, duration: Note.thirtysecond},
                                       {object: "backhead", property: "rightHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: [Note.sixteenth, Note.thirtysecond],
                       failRoutine: 0,
                       animations: [
                                       {object: "backhead", property: "centerX", endValue: 50, duration: Note.thirtysecond}
                                   ]
                   }
               ],
        failRoutines: [
                          [ // 0
                              {object: "1", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "1", property: "centerY", endValue: 550, duration: Note.sixteenth},
                              {object: "2", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "2", property: "centerY", endValue: 553, duration: Note.sixteenth},
                              {object: "2", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "3", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "3", property: "centerY", endValue: 546, duration: Note.sixteenth},
                              {object: "4", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "4", property: "centerY", endValue: 549, duration: Note.sixteenth},
                              {object: "4", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "5", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "5", property: "centerY", endValue: 554, duration: Note.sixteenth},
                              {object: "6", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "6", property: "centerY", endValue: 551, duration: Note.sixteenth},
                              {object: "6", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "7", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "7", property: "centerY", endValue: 548, duration: Note.sixteenth},
                              {object: "8", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "8", property: "centerY", endValue: 552, duration: Note.sixteenth},
                              {object: "8", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "9", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "9", property: "centerY", endValue: 547, duration: Note.sixteenth},
                              {object: "backhead", property: "centerY", endValue: 550, duration: Note.sixteenth},
                              {object: "backhead", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ],
                          [ // 1
                              {object: "1", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "1", property: "centerY", endValue: 550, duration: Note.sixteenth},
                              {object: "2", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "2", property: "centerY", endValue: 553, duration: Note.sixteenth},
                              {object: "2", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "3", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "3", property: "centerY", endValue: 546, duration: Note.sixteenth},
                              {object: "4", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "4", property: "centerY", endValue: 549, duration: Note.sixteenth},
                              {object: "4", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "5", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "5", property: "centerY", endValue: 554, duration: Note.sixteenth},
                              {object: "6", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "6", property: "centerY", endValue: 551, duration: Note.sixteenth},
                              {object: "6", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "7", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "7", property: "centerY", endValue: 548, duration: Note.sixteenth},
                              {object: "8", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "8", property: "centerY", endValue: 552, duration: Note.sixteenth},
                              {object: "8", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "9", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "9", property: "centerY", endValue: 547, duration: Note.sixteenth},
                              {object: "backhead", property: "centerY", endValue: 550, duration: Note.sixteenth},
                              {object: "backhead", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ]
                      ],
        reset: [
                   {object: "1", property: "centerX", endValue: 700, duration: Note.sixteenth},
                   {object: "1", property: "centerY", endValue: 300, duration: Note.sixteenth},
                   {object: "1", property: "width", endValue: 100, duration: Note.sixteenth},
                   {object: "1", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "1", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                   {object: "1", property: "rightHead", endValue: 0, duration: Note.sixteenth},
                   {object: "2", property: "centerX", endValue: 605, duration: Note.sixteenth},
                   {object: "2", property: "centerY", endValue: 325, duration: Note.sixteenth},
                   {object: "2", property: "width", endValue: 0, duration: Note.sixteenth},
                   {object: "2", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "2", property: "rotate", endValue: 90, duration: Note.sixteenth},
                   {object: "3", property: "centerX", endValue: 630, duration: Note.sixteenth},
                   {object: "3", property: "centerY", endValue: 150, duration: Note.sixteenth},
                   {object: "3", property: "width", endValue: 0, duration: Note.sixteenth},
                   {object: "3", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "4", property: "centerX", endValue: 485, duration: Note.sixteenth},
                   {object: "4", property: "centerY", endValue: 125, duration: Note.sixteenth},
                   {object: "4", property: "width", endValue: 0, duration: Note.sixteenth},
                   {object: "4", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "4", property: "rotate", endValue: -90, duration: Note.sixteenth},
                   {object: "5", property: "centerX", endValue: 510, duration: Note.sixteenth},
                   {object: "5", property: "centerY", endValue: 500, duration: Note.sixteenth},
                   {object: "5", property: "width", endValue: 0, duration: Note.sixteenth},
                   {object: "5", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "6", property: "centerX", endValue: 365, duration: Note.sixteenth},
                   {object: "6", property: "centerY", endValue: 525, duration: Note.sixteenth},
                   {object: "6", property: "width", endValue: 0, duration: Note.sixteenth},
                   {object: "6", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "6", property: "rotate", endValue: 90, duration: Note.sixteenth},
                   {object: "7", property: "centerX", endValue: 390, duration: Note.sixteenth},
                   {object: "7", property: "centerY", endValue: 80, duration: Note.sixteenth},
                   {object: "7", property: "width", endValue: 0, duration: Note.sixteenth},
                   {object: "7", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "8", property: "centerX", endValue: 245, duration: Note.sixteenth},
                   {object: "8", property: "centerY", endValue: 55, duration: Note.sixteenth},
                   {object: "8", property: "width", endValue: 0, duration: Note.sixteenth},
                   {object: "8", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "8", property: "rotate", endValue: -90, duration: Note.sixteenth},
                   {object: "9", property: "centerX", endValue: 270, duration: Note.sixteenth},
                   {object: "9", property: "centerY", endValue: 300, duration: Note.sixteenth},
                   {object: "9", property: "width", endValue: 0, duration: Note.sixteenth},
                   {object: "9", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "backhead", property: "centerX", endValue: 50, duration: Note.sixteenth},
                   {object: "backhead", property: "centerY", endValue: 300, duration: Note.sixteenth},
                   {object: "backhead", property: "rotate", endValue: 0, duration: Note.sixteenth},
                   {object: "backhead", property: "rightHead", endValue: 0, duration: Note.sixteenth}
               ]
    },
    { // Level 9
        name: "Riding 2",
        tempo: 110,
        intro: [
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [3], duration: Note.quarter}
               ],
        pattern: [
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.quarter},
                     {channels: [1], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3], duration: Note.quarter},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1], duration: Note.quarter},
                     {channels: [1], duration: Note.eighth},
                     {channels: [1], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3, 8], duration: Note.quarter}
                 ],
        objects: [
                     {name: "prompt", type: Arrow, args: {centerX: 400, centerY: 460, width: 200, height: 50, rotate: 90, leftHead: 10, color: "blue"}}
                 ],
        chart: [
                   {direction: Key.up, duration: [Note.eighth, Note.quarter]},
                   {direction: Key.downright, duration: Note.quarter},
                   {direction: Key.up, duration: Note.eighth},
                   {direction: Key.down, duration: Note.quarter},
                   {direction: Key.up, duration: [Note.eighth, Note.quarter]},
                   {direction: Key.downleft, duration: Note.quarter},
                   {direction: Key.up, duration: Note.eighth},
                   {direction: Key.down, duration: Note.quarter}
               ],
        chartDelay: [Note.whole, Note.whole],
        loops: 3,
        introStepLength: 5,
        steps: [
                   {
                       duration: Note.whole,
                       animations: []
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   // body
                   {
                       duration: Note.quarter,
                       failRoutine: 3,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 200, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 140, duration: Note.sixteenth},
                                       {object: "prompt", property: "rotate", endValue: 50, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 2,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 400, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 460, duration: Note.sixteenth},
                                       {object: "prompt", property: "rotate", endValue: 90, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 140, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 460, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 600, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 140, duration: Note.sixteenth},
                                       {object: "prompt", property: "rotate", endValue: 130, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 4,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       failRoutine: 4,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 400, duration: Note.sixteenth},
                                       {object: "prompt", property: "centerY", endValue: 460, duration: Note.sixteenth},
                                       {object: "prompt", property: "rotate", endValue: 90, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 140, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerY", endValue: 460, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   }
               ],
        failRoutines: [
                          [ // 0
                              {object: "prompt", property: "centerX", endValue: 300, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endValue: 550, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ],
                          [ // 1
                              {object: "prompt", property: "centerX", endValue: 900, duration: Note.eighth},
                              {object: "prompt", property: "centerY", endValue: 100, duration: Note.eighth},
                              {object: "prompt", property: "rotate", endIncrement: 180, duration: Note.eighth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.eighth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.eighth}
                          ],
                          [ // 2
                              {object: "prompt", property: "centerX", endIncrement: 100, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endValue: 550, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ],
                          [ // 3
                              {object: "prompt", property: "centerX", endValue: -100, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endValue: 100, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endValue: -45, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ],
                          [ // 4
                              {object: "prompt", property: "centerX", endIncrement: -100, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endValue: 550, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endValue: 180, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ]
                      ],
        reset: [
                   {object: "prompt", property: "centerX", endValue: 400, duration: Note.sixteenth},
                   {object: "prompt", property: "centerY", endValue: 460, duration: Note.sixteenth},
                   {object: "prompt", property: "width", endValue: 200, duration: Note.sixteenth},
                   {object: "prompt", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                   {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth},
                   {object: "prompt", property: "rotate", endValue: 90, duration: Note.sixteenth}
               ]
    },
    { // Level 10
        name: "Climax",
        tempo: 90,
        intro: [
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.half},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [2], duration: Note.quarter},
                   {channels: [3, 10], duration: Note.eighth}
               ],
        pattern: [
                     {channels: [4], duration: Note.eighth},
                     {channels: [1, 10], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3, 10], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1, 10], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [3, 10], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1, 10], duration: Note.eighth},
                     {channels: [4], duration: Note.eighth},
                     {channels: [1, 10, 11], duration: Note.eighth},
                     {channels: [1, 10, 11], duration: Note.eighth},
                     {channels: [5], duration: Note.sixteenth},
                     {channels: [9], duration: Note.sixteenth},
                     {channels: [6], duration: Note.sixteenth},
                     {channels: [7], duration: Note.sixteenth},
                     {channels: [3, 8], duration: Note.eighth}
                 ],
        objects: [
                     {name: "prompt", type: Arrow, args: {centerX: 150, centerY: 300, width: 200, height: 50, rightHead: 10, color: "blue"}}
                 ],
        chart: [
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.left, duration: Note.quarter},
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.left, duration: Note.quarter},
                   {direction: Key.right, duration: Note.quarter},
                   {direction: Key.left, duration: Note.quarter},
                   {direction: Key.right, duration: Note.eighth},
                   {direction: Key.left, duration: Note.eighth},
                   {direction: Key.right, duration: Note.sixteenth},
                   {direction: Key.left, duration: Note.sixteenth},
                   {direction: Key.right, duration: Note.sixteenth},
                   {direction: Key.left, duration: Note.sixteenth}
               ],
        chartDelay: [Note.whole, Note.whole],
        onlyFinishWhenPracticing: true,
        loops: 6,
        introStepLength: 6,
        hellPeeks: 20,
        hellLevel: 0.95,
        progressiveHell: true,
        steps: [
                   {
                       duration: Note.whole,
                       animations: []
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.quarter,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       finish: tempoUp,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   // body
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       finish: tempoUp,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       finish: tempoUp,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       finish: tempoUp,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       finish: tempoUp,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       finish: tempoUp,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "width", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "height", type: "pulse", pulseFactor: 1.2, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                                   ]
                   },
                   {
                       duration: Note.sixteenth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.thirtysecond},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.sixteenth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 400, duration: Note.thirtysecond},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.sixteenth,
                       failRoutine: 0,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.thirtysecond},
                                       {object: "prompt", property: "leftHead", endValue: 10, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.sixteenth,
                       failRoutine: 1,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 400, duration: Note.thirtysecond},
                                       {object: "prompt", property: "leftHead", endValue: 0, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 10, duration: Note.thirtysecond}
                                   ]
                   },
                   {
                       duration: Note.eighth,
                       failRoutine: 0,
                       finish: tempoUp,
                       animations: [
                                       {object: "prompt", property: "centerX", endValue: 650, duration: Note.thirtysecond},
                                       {object: "prompt", property: "rightHead", endValue: 0, duration: Note.thirtysecond}
                                   ]
                   }
               ],
        failRoutines: [
                          [ // 0
                              {object: "prompt", property: "centerX", endIncrement: 250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: 30, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth}
                          ],
                          [ // 1
                              {object: "prompt", property: "centerX", endIncrement: -250, duration: Note.sixteenth},
                              {object: "prompt", property: "centerY", endIncrement: 75, duration: Note.sixteenth},
                              {object: "prompt", property: "rotate", endIncrement: -30, duration: Note.sixteenth},
                              {object: "prompt", property: "leftHead", endValue: 10, duration: Note.sixteenth},
                              {object: "prompt", property: "rightHead", endValue: 0, duration: Note.sixteenth}
                          ]
                      ],
        reset: [
                   {object: "prompt", property: "centerX", endValue: 150, duration: Note.sixteenth},
                   {object: "prompt", property: "centerY", endValue: 300, duration: Note.sixteenth},
                   {object: "prompt", property: "width", endValue: 200, duration: Note.sixteenth},
                   {object: "prompt", property: "height", endValue: 50, duration: Note.sixteenth},
                   {object: "prompt", property: "leftHead", endValue: 0, duration: Note.sixteenth},
                   {object: "prompt", property: "rightHead", endValue: 10, duration: Note.sixteenth},
                   {object: "prompt", property: "rotate", endValue: 0, duration: Note.sixteenth}
               ]
    },
];

function tempoUp() {
    if (metronome.tempo < 150) {
        metronome.setTempo(metronome.tempo + 2);
    }
}
