Adding Some Spice to Your Marriage
==================================

A rhythm game made for [Strawberry Jam](https://itch.io/jam/strawberry-jam). Playable here: https://chz.itch.io/adding-some-spice-to-your-marriage

~~I'm never going to finish this lmao~~ SCRATCH THAT I SORT OF A LITTLE FINISHED IT. Going to keep working on it though, make the practice mode more helpful specifically. Jam version is 1.0.

This code is released under MIT (see LICENSE.txt).


Image Credits
-------------

For this game I grabbed a few random photos that kind of look like stock photos and added a fake stock photo library watermark on top of them. Because, did you know that the licenses of stock photo libraries tend to explicitly forbid you from using their images with watermarks on them, even if you pay for them but want the watermark? Yeah!

All of my modifications are released under the same licenses as the original, but there's no reason you should actually want any of them.

* ["Couple boudoir" by Joanna M. Foto](http://freestocks.org/photo/couple-boudoir/), released under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
* ["Couple boudoir 2" by Joanna M. Foto](http://freestocks.org/photo/couple-boudoir-2/), released under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)
* ["Couple in a Suite" by Club Med UK](https://www.flickr.com/photos/club-med-discover/15152920580/), released under [CC BY-NC 2.0](https://creativecommons.org/licenses/by-nc/2.0/)
* ["Untitled" by fish NC](https://www.flickr.com/photos/inertia_tw/14531371761/), released under [CC BY-NC-SA 2.0](https://creativecommons.org/licenses/by-nc-sa/2.0/)

I also used ["Decorative Pink Flourish Frame" by GDJ](https://openclipart.org/detail/255201/decorative-pink-flourish-frame) from Openclipart for the "logo."
